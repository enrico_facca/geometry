#
# Restore session and print image with the settings
# ( filename width etc) defined in File/set save options
# 
import sys
import os

# get file session
session_file=os.path.abspath(str(sys.argv[1]))

# restore session and print image 
RestoreSession(session_file,0)
DrawPlots()   
SaveWindow()

sys.exit()
