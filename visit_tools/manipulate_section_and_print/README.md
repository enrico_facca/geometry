#Example on how to manipulate one session to print images wiht similar data.

#1 : Copy the file

cp example1.session example2.session

# and manipuled with a test editor to match the source data.
# In this example

sed -i 's/example1/example2/g' example2.session

# that will match source file example1.vtk and and output file name example1.png .

# 2 : Print using the following command

visit -cli -nowin -s ../restore_print.py example2.session








