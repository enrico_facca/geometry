#
# import sys and append preprocess python subroutine
#
import sys
sys.path.append('../../')
import meshtools as mt


#
# import fortran_pyhton_interface library 
# the geometry python library is sought, by default,
# in $../../build/pyhton/fortran_python_interface/ 
#
sys.path.append('../../../build/python/fortran_python_interface/')
from geometry import ( Abstractgeometry, build_refinement )

#
# get input grid path and output subgrid path
#
fn_grid=sys.argv[1]
fn_subgrid=sys.argv[2]


#
# read grid
# 
[coord,topol,flags]=mt.read_grid(fn_grid)
nnode=len(coord)
ncell=len(topol)
nnodeincell=topol.shape[1]
size_cell=mt.make_size(coord,topol)
bar_cell=mt.make_bar(coord,topol)

#
# init. Fortan structure for grid and subgrid
#
topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1

grid=Abstractgeometry.abs_simplex_mesh()
Abstractgeometry.constructor_from_data(grid, 6, nnode, ncell, nnodeincell, 'triangle', topolT, coordT)

subgrid=Abstractgeometry.abs_simplex_mesh()
build_refinement(0, grid, subgrid)

coordsub=subgrid.coord.transpose()
triangsub=subgrid.topol
triangsub=triangsub[0:3,:].transpose()-1
print(fn_subgrid)
mt.write_grid(coordsub,triangsub,fn_subgrid,'dat')
