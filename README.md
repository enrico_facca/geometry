### What is this repository for? ### 

This repository contains the modules for the description of geometric
objects, in particular graph, triangulation of 2d, 3d and surface
domain. Fortran modules are contained in directory "libsrcs/".
The "tools/" directories contains small program to manipulate
geometrical objects.  See README in there.


* Version 2.0

# How do I get set up? #

## Dependencies
  * PROGRAMS
    * GNU Fortran compiler compatible with FORTRAN 2003 (tested with gfortran v>=4.8).
    Default path is assumed to be "/usr/bin/gfortran"
    * Cmake ( version >= 2.8)  
  * EXTERNAL LIBRARIES
    * Blas library
    * Lapack library
  * EXTERNAL REPOSITORIES; clone the following MANDATORY repositories
      in the parent directory
```
git clone https://gitlab.com/enrico_facca/globals.git
git clone https://gitlab.com/enrico_facca/linear_algebra.git
```

The results should be
```
.
├── geometry
├── globals
└── linear_algebra
```

## Dependencies for fortran-python interface
  * PYTHON >=3.6 and PACKAGES 
    * numpy (version 1.18.2 tested/ version>=1.19.x gave issues )
    * f90wrap 

## Usefull Python Packages
  * meshpy 

# Compile #
## Compile pure Fortran sources

In Unix-base dsystem use the classical Cmake commands to compile
libraries and programs:
```
  mkdir build/;
  cd build;
  cmake ../; 
  make
```

Compiling options are:
* RELEASE (default)
* DEBUG
* IEEE
* USER
that can be invoked adding the cmake flag

```
cmake -DBUILD_TYPE="compiling option" .. ;  
```

## Compile Fortran-python interface ##
In the build/ directory run:

```
make f2py_interface_geometry
```

## Troubleshooting ##

In case your gfortran compiler path is not "/usr/bin/gfortran" pass the absoulte path 

```
cmake -DMy_Fortran_Compiler="absolute path to gfortran" .. ;
```

In case of errors, in particular with the fortran-python interface, try
to pass the path of the directories containg the blas and lapack libraries using the
cmake flag:

```
cmake -DBLAS_DIR="absolute path for blas library" -DLAPACK_DIR="absolute path for lapack library" ..; 
```

In order to diagnose the error sources use also 

```
cmake .. VERBOSE=1;
```

* Configuration
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact