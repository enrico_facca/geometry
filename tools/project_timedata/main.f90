!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> Program for the projection of timedata data defined on grid
!> (ndata=ncell or nnode) 
!> to a coaser grid via the parent file 
!> 
!> (INPUTS:  timedata defined on subgrid, subgrid, parent)
!> (OUTPUTS: timedata defined of grid                     )
!> 
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM project_timedata
  use Globals
  use AbstractGeometry
  use TimeInputs
  use TimeOutputs
  
  implicit none
  
  type(abs_simplex_mesh) :: subgrid,grid

  integer  :: dim,ndata,ndataproj
  logical  :: rc
  integer  :: res
  character(len=256) :: inputs
  character(len=256) :: data_fname
  character(len=256) :: subgrid_fname
  character(len=256) :: grid_fname
  character(len=256) :: parent_fname
  character(len=256) :: dataproj_fname
  character(len=256) :: distinguish_nnode_eq_nncell
  
  type(file) :: fdata
  type(file) :: fsubgrid
  type(file) :: fgrid
  type(file) :: fparent
  type(file) :: fdataproj

  type(TimeData) :: tdata



  integer :: stderr,stdout,debug ,narg
  integer :: i, j,icell,inode, i1,i2
  real(kind=double) :: time,tzero
  real(kind=double),allocatable :: work(:) ,work_prj(:)

  type(TDout) :: dataproj

  logical :: steady,endfile
  logical :: end_reached
  logical :: end_files


  stderr=6
  stdout=6


  !>----------------------------------------------------------------------------
  
  !>----------------------------------------------------------------------------
  !> Geometry info
  !> Read original grid
  ! no renumbering
  ! no connection of second level
  narg=1
  call  get_command_argument(narg, data_fname,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
  narg=narg+1
  call  get_command_argument(narg, subgrid_fname,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
  narg=narg+1
  call  get_command_argument(narg, grid_fname,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  narg=narg+1
  call  get_command_argument(narg, parent_fname,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  narg=narg+1
  call  get_command_argument(narg, dataproj_fname,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  !
  ! init in-out files
  !
  call fsubgrid%init(stderr,etb(subgrid_fname),10,'in')
  call fgrid%init(stderr,etb(grid_fname),11,'in')
  call fparent%init(stderr,etb(parent_fname),12,'in')
  call fdata%init(stderr, etb(data_fname),13,'in')
  call fdataproj%init(stderr,etb(dataproj_fname),14,'out')
  
  !
  ! init grid and subgrid
  !
  call grid%read_mesh(stderr,fgrid)
  call subgrid%read_mesh(stderr,fsubgrid)
  call subgrid%read_parent(stderr,fparent)
  call grid%build_size_cell(stderr)
  call subgrid%build_size_cell(stderr)
  !
  !  distinguish projection
  !
  call tdata%init(stderr, fdata)
  if (  subgrid%ncell .ne.  subgrid%nnode ) then
     if ( tdata%ndata .eq. subgrid%ncell ) then
        !
        ! data_type = 'cell'
        !
        ndataproj = subgrid%ncell_parent
     else if ( tdata%ndata .eq. subgrid%nnode) then
        !
        ! data_type = 'nnode'
        !
        ndataproj = subgrid%nnode_parent
     else
        write(0,*) 'wrong dimensions'
        stop
     end if
  else
     narg=narg+1
     call  get_command_argument(narg, inputs, status=res)
     if (res .ne. 0) call err_handle(stderr,narg)
     read(inputs,*) distinguish_nnode_eq_nncell

     if ( distinguish_nnode_eq_nncell == 'cell' ) then
        !
        ! data_type = 'cell'
        !
        ndataproj = subgrid%ncell_parent
     else if ( distinguish_nnode_eq_nncell == 'node' ) then
        !
        ! data_type = 'nnode'
        !
        ndataproj = subgrid%nnode_parent
     else
        write(0,*) 'wrong distinguish_nnode_eq_nncell, passed = ', &
             etb(distinguish_nnode_eq_nncell)
        stop
     end if
  end if


  !
  ! allocate work variable 
  !
  allocate (work(tdata%ndata),work_prj(ndataproj),stat=res)
  if(res .ne. 0) rc = IOerr(stderr, err_alloc ,&
       'main', &
       ' vars work work_prj', res)


  call dataproj%init(stderr, tdata%dimdata,ndataproj)
  write(fdataproj%lun,*)  dataproj%dimdata,  dataproj%ndata

  ! build
  endfile=.false.
  steady=.false.  
  time = tdata%TDtime(1)
  do while ( (.not. steady ) .and. ( .not. endfile  ) )
     call tdata%set(stderr,fdata,time,endfile)
     dataproj%time = time
     if ( ndataproj .eq. subgrid%ncell_parent ) then
        do i=1,tdata%dimdata
           do j=1,tdata%ndata
              work(j)=tdata%TDactual(i,j)
           end do
           call subgrid%avg_cell_subgrid(grid,work,work_prj)
           do j=1,ndataproj
              dataproj%TDactual(i,j) = work_prj(j)
           end do
        end do
     end if

     if ( ndataproj .eq. subgrid%nnode_parent ) then
        do inode=1,subgrid%nnode
           i1=subgrid%node_parent(1,inode) 
           i2=subgrid%node_parent(2,inode) 
           !
           ! two idential fathers
           !
           if ( (  i1 .eq. i2 ) .and. (i1 .ne. 0) )  dataproj%TDactual(:,i1)=tdata%TDactual(:,inode)
           
           !
           ! two idential fathers
           !
           if ((  i1 .eq. 0  ) .and. ( i2 .ne. 0  ) ) dataproj%TDactual(:,i2)=tdata%TDactual(:,inode)
           if ((  i1 .ne. 0  ) .and. ( i2 .eq. 0  ) ) dataproj%TDactual(:,i1)=tdata%TDactual(:,inode)
        end do
     end if
     call dataproj%write2dat(fdataproj%lun)
     time =  tdata%TDtime(2)
     if ( time .eq. huge) then
        steady = .True.
        write(fdataproj%lun,*) 'time ', huge
     end if
  end do

  !
  ! free memory
  !
  call fsubgrid%kill(stderr)
  call fgrid%kill(stderr)
  call fparent%kill(stderr)
  call fdata%kill(stderr)
  call fdataproj%kill(stderr)
  call grid%kill(stderr)
  call subgrid%kill(stderr)
  
end PROGRAM project_timedata

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./project_timedata.out <data> <subgrid> <grid> <parent> <project_data> [<distinguish>]'
  write(lun_err,*) ' where '
  write(lun_err,*) ' data        (in )         : timedata to be projected ( defined on node or cell)'
  write(lun_err,*) ' finer_grid  (in )         : refined triangulation in ascii'
  write(lun_err,*) ' coaser_grid (in )         : orignal triangulation in ascii'
  write(lun_err,*) ' parent      (in )         : relation grid-subgrid in ascii'
  write(lun_err,*) ' proj_data   (out)         : projected data (according to dimension or flag "distinguish"'
  write(lun_err,*) ' distinguish (in,optional) : "cell" or "node" in case of equal number of nodes and cells'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
