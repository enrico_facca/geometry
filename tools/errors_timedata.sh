#!/bin/bash
#
# compute realtive lp-errors between timedata   
#

#
# usage ./errors_timedata.sh grid approx exact [pnorm]
#
# where:
# grid   [in     ] :: triangulation
# exact  [in     ] :: exact solution path
# approx [in     ] :: approximate solution path
# pnorm  [in, opt] :: pnorm_use (default=2.0) 
#
# result : time evolution of absolute errors.
#

#
# read inputs
# 
grid=$1
approx=$2
exact=$3
p_norm=$4

#
# check data dimension
# 
dim_head=$(head -1 $exact)
dim=${dim_head[0]}

dir=`dirname "$0"`
repo=`eval "cd $dir/../../;pwd;cd - > /dev/null"`



$repo/geometry/statistic_grid/statistic_grid.out ${grid} 'cellsize' cell_size.dat
$repo/globals/tools/lp_error.sh $approx $exact $p_norm 'cell_size.dat' 
rm cell_size.dat
