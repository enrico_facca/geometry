!>---------------------------------------------------------------------
!> \author{Enrico Facca and Mario Putti}
!>
!> 
!> DESCRIPTION: see function help_message 
!> USAGE:       see function help_message 
!>
!>
!> REVISION HISTORY:
!> 07 Dec 2020 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!> TODO_07_DEC_2020 - Check 3d case 
!<---------------------------------------------------------------------

PROGRAM interpolate_neumann
  use Globals
  use AbstractGeometry
  use TimeInputs
  use TimeOutputs
  use SparseMatrix
  
  implicit none
  
  type(abs_simplex_mesh) :: subgrid

  integer  :: dim,ndata,ninterpolated
  logical  :: rc
  integer  :: res
  character(len=256) :: inputs
  character(len=256) :: help_passed
  character(len=256) :: data_fname
  character(len=256) :: subgrid_fname
  character(len=256) :: parent_fname
  character(len=256) :: interpolated_fname
  character(len=256) :: option

  type(file) :: fdata
  type(file) :: fsubgrid
  type(file) :: fparent
  type(file) :: finterpolated

  type(TimeData) :: tdata

  integer :: dimequation

  integer :: stderr,stdout,debug,count !,res
  integer :: i, j,icell,inode,inode_parent,iedge,iface,m
  real(kind=double) :: time,imbalance,tzero,dnrm2
  real(kind=double), allocatable :: v1(:), v2(:)
  real(kind=double), allocatable :: local_data(:),hfunction(:)
  type(TDout) :: interpolated

  logical :: steady,endfile
  logical :: end_reached
  logical :: end_files
  logical :: end_reached_source
  logical :: end_reached_sink  
  logical :: end_reached_dirac_source 
  logical :: end_reached_dirac_sink
  logical :: print_descr

  integer, allocatable :: subgrid_number(:)
  integer, allocatable :: edges(:,:)
  integer, allocatable :: midnode(:)
  integer, allocatable :: face2edge(:,:)
  integer, allocatable :: fileedge_2_parentedge(:)
  
  type(spmat) :: edge2node
  type(spmat) :: node2edge

  integer :: n12,n23,n31,nedge,nmid
  integer :: n1new,n2new,n3new
  integer :: nodeinface(3)
  integer :: n1,n2,n3
  
  stderr=0
  stdout=6


  !>----------------------------------------------------------------------------
  count = command_argument_count()
  if ( count .eq. 1) then
     call get_command_argument(1,inputs, status=res)
     help_passed=etb(inputs)
     if ( help_passed .eq. '--help') then
        print_descr=.True.
        call print_help_message(stdout,print_descr)
        stop
     else
        call print_help_message(stderr,.False.)
     end if
  end if

           


  !>----------------------------------------------------------------------------
  !> Geometry info
  !> Read original grid
  ! no renumbering
  ! no connection of second level   
  call get_command_argument(1,parent_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,.False.)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument parent wrong res= ',res)
  end if

  call get_command_argument(2,data_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,.False.)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument data wrong res= ',res)
  end if
  
  call get_command_argument(3,interpolated_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,.False.)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument data interpolated wrong res= ',res)
  end if

!!$  option='default'
!!$  if (count .eq. 4) then
!!$     call get_command_argument(4,option, status=res)
!!$     if ( res.ne.0) then
!!$        call print_help_message(stderr)
!!$        rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
!!$             '  argument data interpolated wrong res= ',res)
!!$     end if
!!$  end if
  
  call fdata%init(stderr,etb(data_fname),10,'in')
  call fparent%init(stderr,etb(parent_fname),12,'in')
  call finterpolated%init(stderr,etb(interpolated_fname),13,'out')

  !
  ! get grid subgrid relation
  !
  call subgrid%read_parent(stderr,fparent)
 
  !
  ! define relation ordering grid and subgrid
  !
  allocate(&
       subgrid_number(subgrid%nnode_parent),&
       midnode(subgrid%nnode-subgrid%nnode_parent),&
       edges(2,subgrid%nnode-subgrid%nnode_parent),&
       face2edge(3,subgrid%ncell*8),&
       fileedge_2_parentedge(subgrid%nnode-subgrid%nnode_parent),&
       stat=res)
  face2edge=0
  
  m=0
  do inode=1,subgrid%nnode
     if ( subgrid%node_parent(1,inode) .eq. &
          subgrid%node_parent(2,inode) ) then
        m=m+1
        subgrid_number(subgrid%node_parent(1,inode))=inode
     end if
  end do

  nedge=0
  do inode=1,subgrid%nnode
     if ( subgrid%node_parent(1,inode) .ne. &
          subgrid%node_parent(2,inode) ) then
        nedge=nedge+1
        midnode(nedge) = inode
        edges(:,nedge) = &
             (/subgrid%node_parent(1,inode),&
             subgrid%node_parent(2,inode)/)
     end if
  end do
     

  call edge2node%init(stderr,&
       nedge,subgrid%nnode_parent,nedge*2,'csr')
  edge2node%ia(1)=1
  do i=1,nedge
     edge2node%ia(i+1)=edge2node%ia(i)+2
     edge2node%ja(edge2node%ia(i):edge2node%ia(i+1)-1)=edges(:,i)
  end do
  edge2node%coeff=zero

  node2edge=edge2node
  call node2edge%transpose(stderr)
     
  

  call tdata%init(stderr, fdata)
!!$  if ( tdata%dimdata .eq. 5 )  then
!!$     if  ( option == 'default') then
!!$        rc = IOerr(stderr, err_inp , 'interpolate_neumann', &
!!$             ' field <option> must be specified for data with dimension=4 ',res)
!!$     else
!!$        if ( option == '-s') then
!!$           dimequation=1
!!$        else if ( option == '-v') then
!!$           dimension=3
!!$        else
!!$           rc = IOerr(stderr, err_inp , 'interpolate_neumann', &
!!$             ' only "-s" (scalar) and "-v" (vectorial) are admissible for <option>',res)
!!$        end if 
!!$        
!!$     end if
!!$  else
  select case(tdata%dimdata)
  case(3)
     dimequation=1
  case(4)
     if ( subgrid%logical_dimension == 2 ) then 
        dimequation=2
     else if ( subgrid%logical_dimension == 3 ) then
        dimequation=1
     end if
  case (6)
     dimequation=3
  case default
     rc = IOerr(stderr, err_inp , 'interpolate_neumann', &
          ' data '//etb(data_fname)// &
          'has not admissible dimension =',tdata%dimdata)
  end select

  !
  ! number of edges of subgrid via Euler formula
  ! 
  !ninterpolated=subgrid%nnode+subgrid%nnode-2
  ninterpolated=subgrid%ncell+subgrid%nnode-1
  call interpolated%init(stderr, tdata%dimdata,ninterpolated)
  open(finterpolated%lun, file = finterpolated%fn)
  write(finterpolated%lun,*)  interpolated%dimdata,  interpolated%ndata

  allocate(local_data(tdata%dimdata))
  allocate(hfunction(dimequation))
    
  
  ! build
  endfile=.false.
  steady=.false.

    
  time = tdata%TDtime(1)
  do while ( (.not. steady ) .and. ( .not. endfile  ) )
     call tdata%set(stderr,fdata,time,endfile)
     interpolated%time = time
     
     select case ( subgrid%logical_dimension )
     case (2)
        m=0
        do iedge = 1,tdata%ndata
           local_data = tdata%TDactual(:,iedge)
           if (dnrm2(tdata%dimdata,local_data,1)>1e-16) then
              !write(*,*) local_data(1:2)-one
              !
              ! get data 
              !
              n1=int(local_data(1))
              n2=int(local_data(2))
              hfunction = local_data(3:3+dimequation-1)

              !
              ! get corresponding edge index
              ! given by parent
              !
              if ( fileedge_2_parentedge(iedge) .eq. 0) then
                 nodeinface(1:2)=(/n1,n2/)
                 call build_node2edge(edge2node,node2edge,nodeinface(1:2),&
                      fileedge_2_parentedge(iedge))
              end if

              !
              ! index nodes in subgrid 
              !
              n1new=subgrid_number(n1)
              n2new=subgrid_number(n2)
              nmid = midnode(fileedge_2_parentedge(iedge))
              !write(*,*) n1-1,n1new-1
              !write(*,*) n2-1,n2new-1
              !write(*,*) 'mid',nmid-1
              
              !
              ! edge N1-Midnode
              !
              m=m+1           
              interpolated%TDactual(1,m)=real(n1new)
              interpolated%TDactual(2,m)=real(nmid)
              do i=1,dimequation
                 interpolated%TDactual(3+i-1,m)=hfunction(i)
              end do

              !
              ! edge Midnode_N2
              !
              m=m+1
              interpolated%TDactual(1,m)=real(nmid)
              interpolated%TDactual(2,m)=real(n2new)
              do i=1,dimequation
                 interpolated%TDactual(3+i-1,m)=hfunction(i)
              end do
           end if
        end do
     case (3)
        m=0
        do iface = 1,tdata%ndata
           local_data = tdata%TDactual(:,iface)
           if (dnrm2(tdata%dimdata,local_data,1)>1e-16) then

              n1=int(local_data(1))
              n2=int(local_data(2))
              n3=int(local_data(3))
              hfunction = local_data(4:4+dimequation-1)

              n1new=subgrid_number(n1)
              n2new=subgrid_number(n2)
              n3new=subgrid_number(n3)
              if ( face2edge(1,iface) .eq. 0) then
                 nodeinface=(/n1new,n2new,n3new/)
                 call build_face2edge(edge2node,&
                      node2edge,nodeinface,face2edge(:,iedge))
              end if
              n12  = midnode(face2edge(1,iface))
              n23  = midnode(face2edge(2,iface))
              n31  = midnode(face2edge(3,iface))

              m=m+1
              interpolated%TDactual(1:3,m)=(/n1new,n12,n31/)
              interpolated%TDactual(3:3+dimequation-1,m)=hfunction(1:dimequation)

              m=m+1
              interpolated%TDactual(1:3,m)=(/n12,n2new,n23/)
              interpolated%TDactual(3:3+dimequation-1,m)=hfunction(1:dimequation)

              m=m+1
              interpolated%TDactual(1:3,m)=(/n23,n3new,n31/)
              interpolated%TDactual(3:3+dimequation-1,m)=hfunction(1:dimequation)

              m=m+1
              interpolated%TDactual(1:3,m)=(/n12,n23,n31/)
              interpolated%TDactual(3:3+dimequation-1,m)=hfunction(1:dimequation)
           end if
        end do

        
     end select
        
     call interpolated%write2dat(finterpolated%lun)
     time =  tdata%TDtime(2)
     if ( time .eq. huge) then
        steady = .True.
        write(finterpolated%lun,*) 'time ', huge
     end if
  end do

  call fdata%kill(stderr)
  call fsubgrid%kill(stderr)
  call fparent%kill(stderr)
  call finterpolated%kill(stderr)


end PROGRAM interpolate_neumann


subroutine print_help_message(lun,print_description)
  use Globals
  implicit none
  integer, intent(in) :: lun
  logical, intent(in) :: print_description
  !local
  character(len=256) :: msg
  msg=' USAGE: " ./interpolate_timedata.out data parent data_inter <option>"' 
  write(lun,*) etb(msg )
  msg=' - parent     : coarser-finer grids realtions'     
  write(lun,*) etb(msg )
  msg=' - data       : data defined an a coaser grid'
  write(lun,*) etb(msg )
  msg=' - data_inter : ouput data interpolated on finer grid'
  write(lun,*) etb(msg )                                                         
  msg=' or  : " ./interpolate_timedata.out --help " for help and description '
  write(lun,*) etb(msg )

  if (print_description) then
     write(lun,*) 'DESCRIPTION:' 
     write(lun,*) ' Program to interpolate neumann data from '
     write(lun,*) ' a coaser triangulation (2d, 3d, surface)'
     write(lun,*) ' to a finer triangulation'
     write(lun,*) ' Input and Output data are in timedata format'
     write(lun,*) ' Each non-zero line (input and output) are given by :'
     write(lun,*) ' iedge node1 node2 h '
     write(lun,*) ' iedge node1 node2 hx hy '
     write(lun,*) ' iface node1 node2 node3 h '
     write(lun,*) ' iface node1 node2 node3 hx hy hz'
     write(lun,*) ' Edge(face) numbering in <data> is ignored and'
     write(lun,*) ' edge numbering given by parent is used.'

  end if
end subroutine print_help_message

subroutine build_face2edge(edge2node,node2edge,nodeinface,edges)
  use Globals
  use SparseMatrix
  implicit none
  type(spmat), intent(in   ) :: edge2node
  type(spmat), intent(in   ) :: node2edge
  integer,     intent(in   ) :: nodeinface(3)
  integer,     intent(inout) :: edges(3)
  !local
  integer :: iedge,iedge_loc
  integer :: N1,N2,N3
  integer :: E12(2),E23(2),E31(2)
  integer :: nstarN1,nstarN2,nstarN3
  integer :: listN1(100),listN2(100),listN3(100)
  integer :: e1,e2,e3

  N1=nodeinface(1)
  N2=nodeinface(2)
  N3=nodeinface(3)

  E12=(/N1,N2/)
  E23=(/N2,N3/)
  E31=(/N3,N1/)

  call isort(2,E12)
  call isort(2,E23)
  call isort(2,E31)
  
  nstarN1=node2edge%ia(N1+1)-node2edge%ia(N1)
  nstarN2=node2edge%ia(N2+1)-node2edge%ia(N2)
  nstarN3=node2edge%ia(N3+1)-node2edge%ia(N3)

  listN1(1:  nstarN1) =  node2edge%ja(node2edge%ia(N1) : node2edge%ia(N1+1)-1)
  listN2(1:  nstarN2) =  node2edge%ja(node2edge%ia(N2) : node2edge%ia(N2+1)-1)
  listN3(1:  nstarN3) =  node2edge%ja(node2edge%ia(N3) : node2edge%ia(N3+1)-1)

  call find_index_edge(nstarN1, listN1, E12, edge2node,edges(1))
  call find_index_edge(nstarN2, listN2, E23, edge2node,edges(2))
  call find_index_edge(nstarN3, listN3, E23, edge2node,edges(3))

  
end subroutine build_face2edge

subroutine build_node2edge(edge2node,node2edge,nodes_in_edge,index_edge)
  use Globals
  use SparseMatrix
  implicit none
  type(spmat), intent(in   ) :: edge2node
  type(spmat), intent(in   ) :: node2edge
  integer,     intent(in   ) :: nodes_in_edge(2)
  integer,     intent(inout) :: index_edge
  !local
  integer :: iedge,iedge_loc
  integer :: N1,N2,N3
  integer :: E(2)
  integer :: nstarN1
  integer :: listN1(100)
  integer :: e1,e2,e3

  N1=nodes_in_edge(1)
  N2=nodes_in_edge(2)

  E=(/N1,N2/)

  call isort(2,E)
  
  nstarN1=node2edge%ia(N1+1)-node2edge%ia(N1)

  listN1(1:  nstarN1) =  node2edge%ja(node2edge%ia(N1) : node2edge%ia(N1+1)-1)
 
  call find_index_edge(nstarN1, listN1, E, edge2node,index_edge)

  
end subroutine build_node2edge




subroutine find_index_edge(nedges, edges, nodestofind, edge2node,iedge_found)
  use Globals
  use SparseMatrix
  implicit none

  integer,     intent(in) :: nedges
  integer,     intent(in) :: edges(nedges)
  integer,     intent(in) :: nodestofind(2)
  type(spmat), intent(in) :: edge2node
  integer,  intent(inout) :: iedge_found
  !local
  integer :: iedge_loc,iedge,n1,n2
  
  do iedge_loc = 1, nedges
     iedge=edges(iedge_loc)
     n1=edge2node%ja(edge2node%ia(iedge)  )
     n2=edge2node%ja(edge2node%ia(iedge)+1)

     if ( ( n1.eq. nodestofind(1) ) .and. &
          (n2 .eq. nodestofind(2) ) ) then
        iedge_found=iedge
        exit
     end if
  end do

end subroutine find_index_edge
