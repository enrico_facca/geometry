!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> This program converts to vtk datas in timedata format 
!> (repository globals) associated to grids
!> 
!> Moduel Geometry2d is used for all tipo of Triangularization 
!> 2d, 3d and surface. 
!>
!> REVISION HISTORY:
!> dd mm  yyyy - 
!> 14 Gen 2019 - Correction to base program on repository structures 
!>
!> TODO_dd_mmm_yyyy - (TODO_name ) :TODO_describe_appropriate_changes 
!> 14 Gen 2019 - (Enrico Facca ) :  Program should use a general 
!>               module Geometry. It should be exteded or rewritten 
!>               to HDF5 file-format 
!<---------------------------------------------------------------------

PROGRAM timedata2vtk
  use Globals
  use AbstractGeometry  
  use TimeInputs
  use vtkloc

  implicit none
  type(abs_simplex_mesh)     :: grid
  type(file)     :: file_grid,file_vtk
  

  integer :: stderr,stdout,debug, flag_reading
  integer :: i,j,k,l,m, itria,itime
  integer :: ndata,dim,nfiles,start,total_dim,total_ndata
  real(kind=double) :: time,factor
  real(kind=double) , allocatable :: utop(:),zeros(:)
  real(kind=double) , allocatable :: coord_original(:,:)
  
  character(len=256) :: arg

  character(len=256) :: path_grid
  character(len=256) :: path_data
  character(len=256) :: data_type
  character(len=256) :: vtk_folder ,name_vtk  
  character(len=256) :: fname
  character(len=256) :: name_data,vtk_name,var_name,scalar_vector=' '
  character(len=4), allocatable :: total_data_types(:)
  character(len=256), allocatable :: data_paths(:)
  character(len=256), allocatable :: data_names(:)
  character(len=256), allocatable :: total_data_names(:)

  type(file)    ,allocatable      :: data_files(:)
  type(TimeData),allocatable   :: data_inputs(:)
  
  integer, allocatable :: data_dims(:)
  integer, allocatable :: data_ndatas(:)
  integer, allocatable :: total_data_ndatas(:)

  ! local vars
  integer :: u_number
  integer :: res,ncoord,nnodeincell,narg,res_read
  integer :: connection(5)
  logical :: rc,steadyTD
  character(len=256) :: rdwr,str,first_line,dst,base_name,dst_folder,dirname,basename,clean,msg

  logical endfile
  integer ppos,pbar,pdot,ndst,nstr,nargs
  integer :: vector_dimension,ndata_grid,ambient_dimension

  character(len=8) :: label

  stderr = 0

  nargs = command_argument_count() 
  narg = 0
  !if (nargs < 2) call err_handle(stderr,narg)
  
  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_grid
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  write(*,*) etb(path_grid)

 
 
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  read(arg,'(a256)',iostat=res_read) vtk_folder
  if (res_read .ne. 0) call err_handle(stderr,narg)

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  read(arg,'(a256)',iostat=res_read) name_vtk
  if (res_read .ne. 0) call err_handle(stderr,narg)


  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  read(arg,*,iostat=res_read) factor
  if (res_read .ne. 0) call err_handle(stderr,narg)
  
  nfiles = nargs - 4
!!$  narg=narg+1
!!$  call  get_command_argument(narg, arg,status=res)
!!$  if (res .ne. 0) call err_handle(stderr,narg)
!!$  read(arg,*,iostat=res_read) nfiles
!!$  if (res_read .ne. 0) call err_handle(stderr,narg)

  
  allocate(data_paths(nfiles))
  
  do i=1,nfiles
     narg=narg+1
     call  get_command_argument(narg, arg,status=res)
     if (res .ne. 0) call err_handle(stderr,narg)
     read(arg,'(a256)',iostat=res_read) data_paths(i)
     if (res_read .ne. 0) call err_handle(stderr,narg)
     write(*,*) etb(data_paths(i))
  end do



  allocate( data_names(nfiles))
  
  !
  ! get data name
  !
  do i=1,nfiles
     data_names(i)=' '
     clean=etb(data_paths(i))
     ppos = scan(etb(clean),".", BACK= .true.)
     pbar = scan(etb(clean),"/", BACK= .true.)
     data_names(i)=clean(pbar+1:ppos-1)
     write(*,*) etb(data_names(i))
  end do
  
  allocate( data_files(nfiles),data_inputs(nfiles))
  
  !
  ! open input files
  !
  call file_grid%init(stderr,etb(path_grid),10,'in')
  do i=1,nfiles
     call data_files(i)%init(stderr, etb(data_paths(i)), 11+i,'in')
  end do
  
  !
  ! read grid
  !
  call grid%read_mesh(stderr,file_grid)
  ambient_dimension = grid%ambient_dimension
  allocate(coord_original(3,grid%nnode))
  coord_original=grid%coord
  

  !
  ! get dimension datas by the head
  !
  allocate( data_dims(nfiles), data_ndatas(nfiles))
  total_ndata = 0
  total_dim   = 0 
  do i=1,nfiles
     call data_inputs(i)%init(stderr,data_files(i))
     data_dims(i)=data_inputs(i)%dimdata
     data_ndatas(i)=data_inputs(i)%Ndata
     

     dim=data_dims(i)
     ndata=data_ndatas(i)

     
     total_dim  = total_dim + dim
     total_ndata=total_ndata+dim*ndata

     write(*,*) 'total_ndata ',total_ndata 
  end do

  allocate(&
       total_data_names(total_dim),&
       total_data_ndatas( total_dim ),&
       total_data_types( total_dim ))

  k=0
  do i=1,nfiles
     !
     ! check consistency
     !
     dim  = data_dims(i)
     ndata= data_ndatas(i)
     
    
     if (ndata==grid%ncell) then
        vector_dimension=dim
        data_type='cell'
        ndata_grid=grid%ncell
     else if (ndata==grid%nnode) then
        vector_dimension=dim
        data_type='node'
        ndata_grid=grid%nnode
     else
        rc = IOerr(stderr, err_inp , 'main', &
             'file '//etb(data_files(i)%fn)//&
             ' has dimensions not consistent with grid '//&
             etb(file_grid%fn))
     end if

     do j=1,dim
        k=k+1
        total_data_names(k)=' '
        if (dim .eq. 1) then
           total_data_ndatas(k) = ndata
           total_data_names (k) = data_names(i)
           total_data_types(k)  = data_type
        else
           total_data_ndatas(k) = ndata
           total_data_types(k) = data_type
           write(total_data_names(k),'(a,I1)') etb(data_names(i)), j
        end if
        write(*,*) etb(total_data_names(k)),total_data_ndatas(k),total_data_types(k)
     end do
  end do


  allocate(utop(total_ndata))


  endfile=.false.
  time  = data_inputs(1)%TDtime(1)
  do i=2,nfiles
     time=max(time,data_inputs(i)%TDtime(1))
  end do
  itime = 0
  steadyTD=.False.
  write(*,*) 'time',time
  do while ( .not. steadyTD  )
     !
     ! read data at each time
     !
     endfile=.False.
     do i=1,nfiles
        call data_inputs(i)%set(stderr, data_files(i), time, rc)
        if (rc) endfile = .True.
     end do

     !
     ! 
     !
     if ( .not. endfile ) then
        steadyTD=.True.
        time=data_inputs(1)%TDtime(2)
        do i=1,nfiles
           steadyTD=steadyTD.and. data_inputs(i)%steadyTD
           time=min(time,data_inputs(i)%TDtime(2))
        end do
     end if


     !
     ! vtkname will be in format "name0000*.vtk" only
     ! for a sequence of data, otherwise it will be
     ! "name.vtk"
     !
     if ( (itime.eq. 0) .and. ( steadyTD .or. endfile) ) then
        vtk_name=etb(etb(vtk_folder)//etb(name_vtk)//'.vtk')
        write(*,*) 'Steady-state data written in vtk_file= ', etb(vtk_name)
     else              
        write(label,'(i0.8)') itime
        vtk_name=etb(etb(vtk_folder)//etb(name_vtk)//label//'.vtk')
        write(*,*) 'time = ',time, ' endfile= ', endfile, ' vtk_file= ', etb(vtk_name)
     end if
     !
     ! create utop vectors
     !
     start=0
     
     do l=1,nfiles
        ndata=data_inputs(l)%ndata
        dim=data_inputs(l)%dimdata
        !write(*,*) l, dim, ndata, start
        do j=1,ndata
           do i=1,dim             
              k=start+j+(i-1)*ndata
              utop(k)=data_inputs(l)%TDactual(i,j)
           end do
        end do
        start=start+dim*ndata
     end do

!!$     if (itime==0)then
!!$        do i=1,total_dim
!!$           write(777,*) utop(i)
!!$        end do
!!$     end if
     !
     ! write vtk
     !
     grid%coord(1,:)=coord_original(1,:)+factor*utop(1:grid%nnode)
     grid%coord(2,:)=coord_original(2,:)+factor*utop(grid%nnode+1:2*grid%nnode)
     if (grid%logical_dimension==3) &
          grid%coord(3,:)=coord_original(3,:)+&
          factor*utop(2*grid%nnode+1:3*grid%nnode)
     
     call vtkscal(12,vtk_name,'ASCII',real(time),grid, &
          total_dim,total_ndata, total_data_types, total_data_names, utop)

     !
     if ( endfile) then
        exit
     end if
     itime=itime+1
     
  end do
  
  call file_grid%kill(stderr)

  do i=1,nfiles
     call data_files(i)%kill(stderr)
     call data_inputs(i)%kill(stderr)
  end do
  
 end PROGRAM timedata2vtk

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./multiple_timedata2vtk.out <grid> <vtk_folder> <vtk_name> <ndatas> <datas>'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid       (in ): triangulation in ascii'
  write(lun_err,*) ' vtk_dir    (in ): directory for output vtk file'
  write(lun_err,*) ' var_name   (in ): Variable name in vtk file'
  write(lun_err,*) ' scaling    (in ): deformation scaling(0=no grid deformation)'
  write(lun_err,*) ' datas      (in ): timedata files to be converted in vtk '
  write(lun_err,*) '                   first file must be vector field '
  write(lun_err,*) '                   that moves coordinates'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle

