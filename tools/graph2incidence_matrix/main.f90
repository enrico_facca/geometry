!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> This program takes a graph $G$ with nnode-nodes ncell-edges 
!> and it writes on files the transpose of the incidence matrix M
!>
!>   M_{e,i} = -+1 if node "i" belongs edge "e"
!>   
!> and zero otherwise. (The sign is determited by orientation 
!> of the edge. The orientation is from the node with lower 
!> index to higher index), the length of the edges and, optionally,
!> the kernel of the transpose, which is  the constant vector 
!> of lenght nnode.  
!> Use file "example_graph" to test the program.
!> 
!> INPUT : 
!>  graph - ASCII file describing the graph coordinates and topology
!>       
!> OUTPUT :
!>  matrix : transpose of signed incidence matrix
!>  length : lenght of edges
!>  kernel(optional) : kernel of matrix written
!>
!> REVISION HISTORY:
!> 19 Aug 2019 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM graph2incidence_matrix
  use Globals
  use SparseMatrix
  use AbstractGeometry
    
  implicit none
  
  type(file) :: file_graph, file_adj, file_length,file_kernel
  type(spmat) :: sp
  type(abs_simplex_mesh) :: graph
  real(kind=double),allocatable :: length_edges(:),rhs(:)

  logical :: rc ,write_kernel
  integer :: stderr,stdout,lun,res,res_read
  integer :: i,j,narg
  integer :: nodes(2)

  character(len=256) :: path_graph
  character(len=256) :: path_adj
  character(len=256) :: path_length
  character(len=256) :: path_kernel
  character(len=256) :: arg
  
  real(kind=double) :: dnrm2

  stderr = 0
  stdout = 0

  !
  ! input file: graph
  !
  narg=1
  call  get_command_argument(narg, path_graph,status=res) 
  if (res .ne. 0) call err_handle(stderr,narg)
  
  !
  ! ouput file : adjacency matrix + edge length + kernel if required
  !
  narg=narg+1   
  call  get_command_argument(narg, path_adj,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
  narg=narg+1   
  call  get_command_argument(narg, path_length,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  narg=narg+1   
  call  get_command_argument(narg, arg,status=res)
  if (res .eq. 0) then
     write_kernel =.True.
     read (arg,'(a256)',iostat=res_read) path_kernel 
     if ( res_read .ne. 0) call err_handle(stderr,narg)
  else
     write_kernel =.False.
  end if

  !
  ! read graph
  !
  call file_graph%init(stderr, etb(path_graph), 10,'in')
  call graph%read_mesh(stderr,file_graph)
  call file_graph%kill(stderr)

  !
  ! init, build, and write the transpose of incident matrix
  !
  call sp%init(stderr,&
       graph%ncell,graph%nnode,graph%ncell*2,&
       'csr',&
       is_symmetric=.False.)
  

  sp%ia(1) = 1
  do i=1,sp%nrow
     sp%ia(i+1) = sp%ia(i)+2 
     nodes=graph%topol(1:2,i)
     call isort(2,nodes)
     do j = 1, 2
        sp%ja(sp%ia(i)+j-1) = nodes(j)
        sp%coeff(sp%ia(i)+j-1) = -(-one)**j
     end do
  end do 
  call sp%transpose(0)


  call file_adj%init(stderr, etb(path_adj), 11,'out')
  call sp%write(file_adj%lun)
  call file_adj%kill(stderr)


  !
  ! compute and write length of edges
  !
  allocate(&
       length_edges(graph%ncell),&
       rhs(graph%nnode),&
       )
  do i=1,graph%ncell   
     length_edges(i) = dnrm2(3,&
          graph%coord(:,graph%topol(1,i)) - &
          graph%coord(:,graph%topol(2,i)),1)
  end do

  call file_length%init(stderr, etb(path_length), 11,'out')
  write(file_length%lun,*) 1, graph%ncell
  write(file_length%lun,*) 'time -1.0e30'
  write(file_length%lun,*) graph%ncell
  do i=1,graph%ncell
     write(file_length%lun,*) i, length_edges(i)
  end do
  write(file_length%lun,*) 'time 1e30'
  call file_length%kill(stderr)
  
  !
  ! optinally write kernel of transpose of incidence matrix
  !
  if ( write_kernel ) then
     call file_kernel%init(stderr, etb(path_kernel), 11,'out')
     write(file_kernel%lun,*) 1, graph%nnode, ' ! dim kernel, size kernel=matrix ncol '
     do i=1,graph%nnode
        write(file_kernel%lun,*) one
     end do
     call file_kernel%kill(stderr)
  end if

end PROGRAM graph2incidence_matrix

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  
  
  write(lun_err,*) ' Error passing arguments, correct usage: '
  write(lun_err,*) ' ./graph2incidence_matrix.out <graph> <matrix> <length> [<kernel>]'
  write(lun_err,*) ' where '
  write(lun_err,*) ' graph    (in )     : graph structure in ascii'
  write(lun_err,*) ' matrix   (out)     : matrix A = transpose of signed incidence matrix'
  write(lun_err,*) ' lenght   (out)     : length of the edges '
  write(lun_err,*) ' kernel   (out,opt) : kernel of matrix A'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Error reading argument number : ', narg)
end subroutine err_handle
