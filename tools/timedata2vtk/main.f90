!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> This program converts to vtk datas in timedata format 
!> (repository globals) associated to grids
!> 
!> Moduel Geometry2d is used for all tipo of Triangularization 
!> 2d, 3d and surface. 
!>
!> REVISION HISTORY:
!> dd mm  yyyy - 
!> 14 Gen 2019 - Correction to base program on repository structures 
!>
!> TODO_dd_mmm_yyyy - (TODO_name ) :TODO_describe_appropriate_changes 
!> 14 Gen 2019 - (Enrico Facca ) :  Program should use a general 
!>               module Geometry. It should be exteded or rewritten 
!>               to HDF5 file-format 
!<---------------------------------------------------------------------

PROGRAM timedata2vtk
  use Globals
  use AbstractGeometry  
  use TimeInputs
  use vtkloc

  implicit none
  type(abs_simplex_mesh)     :: grid
  type(file)     :: file_grid, file_data, file_vtk
  type(TimeData) :: data_input

  integer :: stderr,stdout,debug, flag_reading
  integer :: i,j,k, itria,itime
  integer :: ndata,dim
  integer,allocatable :: data_dims(:)
  real(kind=double) :: time
  real(kind=double) , allocatable :: utop(:),zeros(:)

  character(len=256) :: arg

  character(len=256) :: path_grid
  character(len=256) :: path_data
  character(len=256) :: data_type
  character(len=256) :: vtk_folder   
  character(len=256) :: fname
  character(len=256) :: name_data,vtk_name,var_name,scalar_vector=' '
  character(len=256), allocatable :: data_types(:), names_data(:)

  ! local vars
  integer :: u_number
  integer :: res,ncoord,nnodeincell,narg,res_read
  integer :: connection(5)
  logical :: rc
  character(len=256) :: rdwr,str,first_line,dst,base_name,dst_folder,dirname,basename,clean,msg

  logical endfile
  integer ppos,pbar,pdot,ndst,nstr,nargs
  integer :: vector_dimension,ndata_grid,ambient_dimension

  character(len=8) :: label

  stderr = 0

  nargs = command_argument_count() 
  narg = 0
  if (nargs < 2) call err_handle(stderr,narg)
  
  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_grid
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  read(arg,'(a256)',iostat=res_read) path_data
  if (res_read .ne. 0) call err_handle(stderr,narg)

 
  if (nargs .ge. 3) then
     narg=narg+1
     call  get_command_argument(narg, arg,status=res)
     if (res .ne. 0) call err_handle(stderr,narg)
     read(arg,'(a256)',iostat=res_read) vtk_folder
     if (res_read .ne. 0) call err_handle(stderr,narg)
  else
     !
     ! send vtk folder locally
     !
     vtk_folder='./'
  end if
  
  if (nargs .ge. 4) then
     narg=narg+1
     call  get_command_argument(4, arg,status=res)
     if (res .ne. 0) call err_handle(stderr,narg)
     read(arg,'(a256)',iostat=res_read) var_name
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  scalar_vector='-v'
  if (nargs .eq. 5) then
     narg=narg+1
     call  get_command_argument(narg, arg,status=res)
     if (res .ne. 0) call err_handle(stderr,narg)
     read(arg,'(a256)',iostat=res_read) scalar_vector
     write(*,* )  etb(scalar_vector)
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  

  
  !
  ! get data name
  !
  clean=etb(path_data)
  ppos = scan(etb(clean),".", BACK= .true.)
  pbar = scan(etb(clean),"/", BACK= .true.)
  name_data=clean(pbar+1:ppos-1)

  
  !
  ! if vtk_folder ends  with '.vtk' 
  !    basename = the name with  '.vtk' removed
  !    dst_folder  = the name passed in the name ('./' is the default) 
  !
  nstr=len(etb(vtk_folder))
  if ( vtk_folder(nstr-3:nstr) .eq. '.vtk' ) then
     pbar = scan(etb(vtk_folder),"/", BACK= .true.)
     pdot = scan(etb(vtk_folder),".", BACK= .true.)
     basename=vtk_folder(pbar+1:pdot-1)
     if (pbar .eq. 0 ) then
        dst_folder='.'
     else
        dst_folder=etb(get_dirname(vtk_folder))
     end if
  end if
  
  !
  ! if the last character if '/' 
  ! basename   = name_data
  ! dst_folder = is the folder passed 
  !
  clean=etb(vtk_folder)
  if ( clean(nstr:nstr) .eq. '/' ) then
     dst_folder=etb(vtk_folder(1:nstr-1))
     basename=etb(name_data)
  end if
  
  !
  ! open input files
  !
  call file_grid%init(stderr,etb(path_grid),10,'in')
  call file_data%init(stderr, etb(path_data), 11,'in')

  
  !
  ! read grid
  !
  call grid%read_mesh(stderr,file_grid)
  ambient_dimension = grid%ambient_dimension
  

  !
  ! get dimension data by the head
  !
  call data_input%init(stderr,file_data)
  dim=data_input%dimdata
  ndata=data_input%Ndata

  
  
  !
  ! check consistency
  !
  write(*,*) ndata,grid%nnode, grid%ncell,mod(ndata,grid%nnode),mod(ndata,grid%ncell)
  if (       ( mod(ndata,grid%nnode) .eq. 0 ) &
       .and. ( mod(ndata,grid%ncell) .eq. 0 ) ) then
     if (grid%ncell==grid%nnode) then
        rc = IOerr(stderr, err_inp , 'main', &
             'not able to tell if file '//etb(file_data%fn)//&
             'contians cell or node data for grid'//&
             etb(file_grid%fn))
     else
        if (ndata==grid%ncell) then
           vector_dimension=dim
           data_type='cell'
           ndata_grid=grid%ncell
        else
           vector_dimension=dim
           data_type='node'
           ndata_grid=grid%nnode
        end if
     end if
  else
     if ( mod(ndata,grid%nnode) .eq. 0 ) then
        vector_dimension=(ndata/grid%nnode)*dim
        data_type='node'
        ndata_grid=grid%nnode
     else
        if ( mod(ndata,grid%ncell) .eq. 0 ) then
           vector_dimension=(ndata/grid%ncell)*dim
           data_type='cell'
           ndata_grid=grid%ncell
        else
           rc = IOerr(stderr, err_inp , 'main', &
                'file '//etb(file_data%fn)//&
                ' has dimensions not consistent with grid '//&
                etb(file_grid%fn))
        end if
     end if
  end if
  
  write(*,*) vector_dimension,etb(data_type),ndata_grid,ndata

  !
  ! allcoate work arrays
  !
  allocate(data_dims(dim),&
       data_types(dim),&
       names_data(dim),&
       utop(dim*ndata_grid),&
       zeros(ndata_grid))
  zeros=zero
  data_types(:)= etb(data_type)
  data_dims(:) = ndata
  
  if (nargs .eq. 4) name_data=var_name

  if (dim .eq. 1) then 
     names_data(1)=etb(name_data)
  else
     do i=1,dim
        write(names_data(i),'(a,I1)') etb(name_data), i
     end do
  end if


  endfile=.false.
  time  = data_input%TDtime(1)
  itime = 0
  do while ( &
       ((time .eq. data_input%TDtime(1)) .or. &
       ( .not. data_input%steadyTD) ))
     !
     ! read data at each time
     !
     call data_input%set(stderr, file_data, time, endfile)
     
     !
     ! vtkname will be in format "name0000*.vtk" only
     ! for a sequence of data, otherwise it will be
     ! "name.vtk"
     !
     if ( (itime.eq. 0) .and. ( data_input%steadyTD .or. endfile) ) then
        vtk_name=etb(etb(dst_folder)//'/'//etb(basename)//'.vtk')
        write(*,*) 'Steady-state data written in vtk_file= ', etb(vtk_name)
     else              
        write(label,'(i0.8)') itime
        vtk_name=etb(etb(dst_folder)//'/'//etb(basename)//label//'.vtk')
        write(*,*) 'time = ',time, ' endfile= ', endfile, ' vtk_file= ', etb(vtk_name)
     end if
     !
     ! create utop vectors
     !
     do j=1,ndata_grid
        do i=1,dim
           k=j+(i-1)*ndata_grid
           utop(k)=data_input%TDactual(i,j)
        end do
     end do
     
     !
     ! write vtk
     !
     write(*,*) vector_dimension,dim,ndata_grid,etb(scalar_vector)
     select case( vector_dimension )
     case (1)
        if ( dim > 1) then          
           select case( scalar_vector )
           case ('-s')
              call vtkscal(12,vtk_name,'ASCII',real(time),grid, &
                dim,dim*ndata, data_types, names_data, utop)
           case ('-v')
              if ( dim .eq. 2 ) then
                 write(*,*) vector_dimension,dim,ndata_grid,etb(scalar_vector)

                 call vtkvectors(12,vtk_name,'ASCII',real(time),grid, &
                      ndata_grid, data_type, name_data, &
                      utop(1:ndata_grid), utop(ndata_grid+1:2*ndata_grid),&
                      zeros)
              else 
                 call vtkvectors(12,vtk_name,'ASCII',real(time),grid, &
                      ndata_grid, data_type, name_data, &
                      utop(1:ndata_grid), &
                      utop(ndata_grid+1:2*ndata_grid),&
                      utop(2*ndata_grid+1:3*ndata_grid))
              end if
                 
           end select
        else
           call vtkscal(12,vtk_name,'ASCII',real(time),grid, &
                dim,dim*ndata, data_types, names_data, utop)
        end if
        
     case ( 2 )
!!$        do i=1,ndata_grid
!!$           if ( abs(utop(i) )>0.0 ) write(*,*) i,utop(i)
!!$        end do
!!$        do i=1+ndata_grid,2*ndata_grid
!!$           if ( abs(utop(i) )>0.0 ) write(*,*) i,utop(i)
!!$        end do
        call vtkvectors(12,vtk_name,'ASCII',real(time),grid, &
          ndata_grid, data_type, name_data, &
          utop(1:ndata_grid), utop(ndata_grid+1:2*ndata_grid),zeros)
     case (3)
        call vtkvectors(12,vtk_name,'ASCII',real(time),grid, &
          ndata_grid, data_type, name_data, &
          utop(1:ndata_grid), &
          utop(ndata_grid+1:2*ndata_grid),&
          utop(2*ndata_grid+1:3*ndata_grid))
     end select
        
     if ( endfile) then
        exit
     end if      
     itime= itime + 1
     time = data_input%TDtime(2)
   end do
  
   call file_grid%kill(stderr)
   call file_data%kill(stderr)
  
 end PROGRAM timedata2vtk

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./timedata2vtk_grid.out <grid> <data> [<vtk_dest> <var_name> <scalar_vector>]'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid     (in )      : triangulation in ascii'
  write(lun_err,*) ' data     (in )      : timedata to be converted defined on cell or nodes'
  write(lun_err,*) ' vtk_dest (in )      : 1 - if not passed vtk are created locally'
  write(lun_err,*) '          (optional) :     with same name of the data'
  write(lun_err,*) '                     : 2 - if a directory (end in "/") is passed '
  write(lun_err,*) '                     :     vtk files are created in "path"'
  write(lun_err,*) '                     :     with the same name of data'
  write(lun_err,*) '                     : 3 - if a path in form path/name.vtk is passed, '
  write(lun_err,*) '                     :     vtk files are in "path" with name "name"'
  write(lun_err,*) ' var_name (in )      : Variable name in vtk file'
  write(lun_err,*) '          (optional)'
  write(lun_err,*) ' -s or -v (in )      : how to handle vectorial data'
  write(lun_err,*) '          (optional)'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle

