!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> This program converts to vtk datas in timedata format 
!> (repository globals) associated to grids
!> 
!> Moduel Geometry2d is used for all tipo of Triangularization 
!> 2d, 3d and surface. 
!>
!> REVISION HISTORY:
!> dd mm  yyyy - 
!> 14 Gen 2019 - Correction to base program on repository structures 
!>
!> TODO_dd_mmm_yyyy - (TODO_name ) :TODO_describe_appropriate_changes 
!> 14 Gen 2019 - (Enrico Facca ) :  Program should use a general 
!>               module Geometry. It should be exteded or rewritten 
!>               to HDF5 file-format 
!<---------------------------------------------------------------------

PROGRAM data2vtk
  use Globals
  use AbstractGeometry
  use vtkloc

  implicit none
  type(abs_simplex_mesh)     :: grid
  type(file)     :: file_grid, file_data, file_vtk

  integer :: stderr,stdout,debug, flag_reading
  integer :: i,j,k, itria,itime
  integer :: ndata,dim
  integer,allocatable :: data_dims(:)
  real(kind=double) :: time
  real(kind=double) , allocatable :: data(:,:), utop(:)

  character(len=256) :: arg

  character(len=256) :: path_grid
  character(len=256) :: path_data
  character(len=256) :: data_type
  character(len=256) :: try,vtk_folder   
  character(len=256) :: fname_vtk,fname
  character(len=256) :: name_data,vtk_name
  character(len=256), allocatable :: data_types(:), names_data(:)

  ! local vars
  integer :: u_number
  integer :: res,ncoord,nnodeincell
  integer :: connection(5)
  logical :: rc
  character(len=256) :: rdwr,str,first_line

  logical endfile
  integer ppos,pbar

  character(len=8) :: label

  stderr = 6

  CALL getarg(1, arg)
  read(arg,'(a256)') path_grid

  CALL getarg(2, arg)
  read(arg,'(a256)') path_data

  file_data%fn=etb(path_data)
  file_data%lun=11

  ! set data  name
  ppos = scan(trim(file_data%fn),".", BACK= .true.)
  pbar = scan(trim(file_data%fn),"/", BACK= .true.)
  name_data=file_data%fn(pbar+1:ppos-1)

  CALL getarg(3, arg)
  read(arg,'(a256)',iostat=res) try
  if ( res .eq. 0) then
     fname_vtk=etb(try)
  else
     vtk_folder='./'
     vtk_name=etb(etb(name_data)//'.vtk')
     fname_vtk=etb(etb(vtk_folder)//etb(vtk_name))
  end if

  


  ! grid reading
  call   file_grid%init(0,etb(path_grid),10,'in')
  
  call grid%read_mesh(0,file_grid)


  
  !
  ! read data
  !
  ! open file
  open(file_data%lun,file=file_data%fn)
  ! read dimension
  read(file_data%lun,*) dim, ndata 
  allocate( data(dim,ndata) )
  do i=1,ndata
     read(file_data%lun,*) (data(j,i), j=1,dim)
  end do
  close(file_data%lun)

  !> select type of data
  if ( ndata .eq. grid%nnode) then
     data_type='node' 
  else if ( ndata .eq. grid%ncell ) then
     data_type='cell'
  else
     write(0,*) &
          ' ndata =', ndata, &
          ' nnode=', grid%nnode,&
          ' ncell=', grid%ncell
     
     rc = IOerr(0, err_inp, 'init_spmat', &
          'dimension mismatch')
  end if

  write(*,*) 'number of ',etb(data_type),'=', ndata

  ! set variables names
  allocate(&
       data_dims(dim),&
       data_types(dim),&
       names_data(dim),&
       utop(dim*ndata))
  data_types(1:dim)= data_type
  data_dims(1:dim) = ndata
  if ( dim .eq. 1) then
     write(names_data(1),'(a)') etb(name_data)
  else
     do i=1,dim
        write(names_data(i),'(a,I1)') etb(name_data), i
     end do
  end if

  do j=1,ndata
     do i=1,dim
        k=j+(i-1)*ndata
        utop(k)=data(i,j)
     end do
  end do

  call vtkscal(100,etb(fname_vtk),'ASCII',real(time),grid, &
       dim,dim*ndata, data_types, names_data, utop)

  deallocate(&
       data,&
       data_dims,&
       data_types,&
       names_data,&
       utop)
  
  
end PROGRAM data2vtk
