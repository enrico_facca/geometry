### What is this directory for? ###

This folder contains a series of small program to manipulate
geometrical objects (graphs, triangualtions, etc) and data
defined on them.

For example:
* timedata2vtk takes a grid (graph or a triangulation) and a
timedata file (see globals repository) and it convert it in
vtk format.
* refine takes a triangulation (2d, 3d or surface) and
produce a new grid which is the conformal refinement. More
the one level of refinement is possible.

### How do I get set up? ###
In "build/" directory compile using
```
make tools_geometry
```

### How to use these program? ###

Check the input/output interface
calling each exacutable in build/tools/ with "--help" flag.
