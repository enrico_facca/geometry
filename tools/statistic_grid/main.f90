!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> The main program 
!> 
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM interpolate_timedata
  use Globals
  use AbstractGeometry
  use TimeInputs
  use TimeOutputs
  
  implicit none
  
  type(abs_simplex_mesh) :: grid

  integer  :: dim,ndata,ndataref
  logical  :: rc
  integer  :: res,lun
  character(len=256) :: inputs
  character(len=256) :: data_fname
  character(len=256) :: grid_fname
  character(len=256) :: parent_fname
  character(len=256) :: dataref_fname
  character(len=256) :: command
  character(len=256) :: clean_command
  character(len=256) :: path_out



  type(file)     :: file_cell_size
  
  type(file) :: fdata
  type(file) :: fgrid
  type(file) :: fparent
  type(file) :: fdataref

  type(TimeData) :: tdata



  integer :: stderr,stdout,debug !,res
  integer :: i, j,icell
  real(kind=double) :: time,imbalance,tzero

  type(TDout) :: dataref

  logical :: steady,endfile
  logical :: end_reached
  logical :: end_files
  logical :: end_reached_source
  logical :: end_reached_sink  
  logical :: end_reached_dirac_source 
  logical :: end_reached_dirac_sink   


  stderr=6
  stdout=6


  !>----------------------------------------------------------------------------
  
  !>----------------------------------------------------------------------------
  !> Geometry info
  !> Read original grid
  ! no renumbering
  ! no connection of second level   
  call get_command_argument(1,grid_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument grid wrong res= ',res)
  end if

  call get_command_argument(2,command, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument grid wrong res= ',res)
  end if
  clean_command = etb(command)  
  
  select case ( clean_command) 
  case ('meshpar')
     
     call fgrid%init(stderr,etb(grid_fname),11,'in')

     ! init grid
     call grid%read_mesh(stderr,fgrid)


!!$     write(*,*) ' MeshPar=sqrt(2*sum(this%size_cell)/this%ncell)', grid%meshpar(0)
!!$     write(*,*) ' MeshPar=minval(sqrt(this%size_cell)/2.0d0)    ', grid%meshpar(1)
!!$     write(*,*) ' MeshPar=maxval(sqrt(this%size_cell)/2.0d0)    ', grid%meshpar(2)
!!$     write(*,*) ' MeshPar=sum(this%length_edge)*one/this%nedge  ', grid%meshpar(3)
!!$     write(*,*) ' MeshPar=minval(this%length_edge)              ', grid%meshpar(4)
!!$     write(*,*) ' MeshPar=maxval(this%length_edge)              ', grid%meshpar(5)
 
     call grid%kill(stderr)
     call fgrid%kill(stderr)
  case ('cellsize')
     call fgrid%init(stderr,etb(grid_fname),11,'in')
     ! init grid
     call grid%read_mesh(stderr,fgrid)
     call grid%build_size_cell(stderr)

     call get_command_argument(3,path_out, status=res)
     call file_cell_size%init(stderr,etb(path_out),11,'out')
     lun=file_cell_size%lun
     write(lun,*) '1  ', grid%ncell
     write(lun,*) 'time -1.0e30'
     write(lun,*) grid%ncell
     do icell = 1,grid%ncell
        write(lun,*) icell, grid%size_cell(icell)
     end do
     write(lun,*) 'time 1.0e30'
     call file_cell_size%kill(stderr)

     call grid%kill(stderr)
     call fgrid%kill(stderr)
     
  end select
     
  

  
  contains
    subroutine print_help_message(lun)
      implicit none
      integer, intent(in) :: lun
      !local
      character(len=256) :: msg
      msg=' Usage: ./statistic_grid.out grid'
      write(*,*) msg 
      
    end subroutine print_help_message

  

end PROGRAM interpolate_timedata
