!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> This program converts to vtk datas in timedata format 
!> (repository globals) associated to grids
!> 
!> Moduel Geometry2d is used for all tipo of Triangularization 
!> graph 2d, 3d and surface. 
!>
!> REVISION HISTORY:
!> dd mm  yyyy - 
!> 14 Gen 2019 - Correction to base program on repository structures 
!>
!> TODO_dd_mmm_yyyy - (TODO_name ) :TODO_describe_appropriate_changes 
!> 14 Gen 2019 - (Enrico Facca ) :  Program should use a general 
!>               module Geometry. It should be exteded or rewritten 
!>               to HDF5 file-format 
!<---------------------------------------------------------------------

PROGRAM grid2vtk
  use Globals
  use AbstractGeometry
  use vtkloc

  implicit none
  type(abs_simplex_mesh)     :: grid
  type(file)     :: file_grid,  file_vtk, file_grid_out

  integer :: stderr,stdout,debug, flag_reading
  integer :: i,j,k, itria,itime
  integer :: ndata,dim
  integer,allocatable :: data_dims(:)
  real(kind=double) :: time
  real(kind=double) , allocatable :: data(:,:), utop(:)

  character(len=256) :: arg

  character(len=256) :: path_grid
  character(len=256) :: path_data
  character(len=256) :: data_type
  character(len=256) :: try,vtk_folder   
  character(len=256) :: fname
  character(len=256) :: name_data,vtk_name
  character(len=256), allocatable :: data_types(:), names_data(:)

  ! local vars
  integer :: u_number
  integer :: res,ncoord,nnodeincell
  integer :: connection(5),narg,len
  logical :: rc
  character(len=256) :: rdwr,str,first_line,out_format,input,command_line

  logical endfile
  integer ppos,pbar

  character(len=8) :: label

  stderr=0
  
  !
  ! read input
  !
  narg=0
  call get_command (input, len, status=res)
  command_line=input(1:len)
  if (res .ne. 0) call err_handle(stderr,narg,command_line)
    

  narg=1
  call  get_command_argument(narg, path_grid,status=res)
  if (res .ne. 0) call err_handle(stderr,narg,command_line)


  ! grid reading
  call file_grid%init(6,etb(path_grid),10,'in')
  call grid%read_mesh(6, file_grid)
  call file_grid%kill(6)

 
  narg=narg+1   
  call  get_command_argument(narg, arg, status=res)
  if (res .ne. 0) call err_handle(stderr,narg,command_line)
  read(arg,'(a256)',iostat=res) try
  if ( res .eq. 0) then
     fname=etb(try)
  else
     ! set vtk file name
     ppos = scan(file_grid%fn,".", BACK= .true.)
     fname=etb(etb(file_grid%fn(1:ppos-1))//'.vtk')
  end if

  !
  ! try to read out format defuat=vtk
  !
  out_format='vtk'  
  narg=narg+1   
  call  get_command_argument(narg, arg, status=res)
  if ( res .eq. 0 ) then
     out_format=etb(arg)
  else
     out_format='vtk'
  end if
  
  select case (out_format)
  case ('vtk')
     call vtkmesh(100,etb(fname),'ASCII',real(time),grid)
  case ('gid')
     call file_grid_out%init(6,etb(fname),12,'out')
     call grid%write_mesh(6,file_grid_out,'gid')
     call file_grid_out%kill(6)
  case ('off')
     call file_grid_out%init(6,etb(fname),12,'out')
     call grid%write_mesh(6,file_grid_out,'off')
     call file_grid_out%kill(6)
  case default
     write(*,*) 'Format =', etb(out_format), 'not supported'
  end select

  write(*,*) 'Grid written in ', etb(fname)

  

  
end PROGRAM grid2vtk

subroutine err_handle(lun_err, narg,command_line)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  character(len=*), intent(in) :: command_line
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments'
  write(lun_err,*) ' Command passed: (',etb(command_line), ' )'
  write(lun_err,*) ' Correct usage:'
  write(lun_err,*) '  grid2vtk.out <grid> <grid_out> [<format]>'
  write(lun_err,*) ' where : '
  write(lun_err,*) ' grid      (in )    : triangualtion in ASCII'
  write(lun_err,*) ' grid_out  (out )   : triangualtion in .vtk, .off, .gid '
  write(lun_err,*) ' format    (in,opt) : format (default=vtk)'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
