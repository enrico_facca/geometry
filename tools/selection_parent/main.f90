!>---------------------------------------------------------------------
!> \author{Enrico Facca}
!>
!> DESCRIPTION: 
!> This program that given (INPUTS):
!> 1 - parent file
!> 2 - node selector for coaser grid
!> 3 - cell selector for coaser grid
!> return (OUTPUTS):
!> 1 - parent file   for selected subgrid
!> 2 - node selector for selected subgrid
!> 3 - cell selector for selected subgrid
!>
!> usage ./selection_grid.out "parent" "node_selector" "cell_selector" 
!>                            "parent_selected" "node_selector_subgrid" 
!>                             "cell_selector_subgrid " 
!>
!>
!> REVISION HISTORY:
!> yyyymmdd - 
!> 20190507 - Creation of program
!<---------------------------------------------------------------------

PROGRAM selection_parent 
  use Globals
  use AbstractGeometry
  use TimeInputs
  use vtkloc

  implicit none
  type(abs_simplex_mesh) :: subgrid
  type(abs_simplex_mesh) :: selected_subgrid
  type(file)     :: file_parent
  type(file)     :: file_grid_node_selector
  type(file)     :: file_grid_cell_selector
  type(file)     :: file_subgrid_node_selector
  type(file)     :: file_subgrid_cell_selector
  type(file)     :: file_selected_subgrid_parent
  
  integer :: stderr=0,stdout=6,res,res_read,narg
  integer :: i,j,k,m, icell, inode,itime
  integer :: icell_original,icell_selected, ifather_original
  integer :: inode_original,inode_selected
  integer :: n1_father_original,n2_father_original

  integer :: original_grid_nnode
  integer :: selected_grid_nnode
  integer :: original_grid_ncell
  integer :: selected_grid_ncell
  integer :: original_subgrid_nnode
  integer :: selected_subgrid_nnode
  integer :: original_subgrid_ncell
  integer :: selected_subgrid_ncell



  character(len=256) :: arg
  character(len=256) :: path_parent
  character(len=256) :: path_grid_node_selector
  character(len=256) :: path_grid_cell_selector
  character(len=256) :: path_subgrid_node_selector
  character(len=256) :: path_subgrid_cell_selector
  character(len=256) :: path_selected_subgrid_parent


  logical :: rc
  logical endfile,endreading,makeother
  integer info
  
  integer ,allocatable :: grid_node_selector(:)
  integer ,allocatable :: grid_cell_selector(:)
  integer ,allocatable :: subgrid_node_selector(:)
  integer ,allocatable :: subgrid_cell_selector(:)


  integer ,allocatable :: inv_grid_node_selector(:), inv_grid_cell_selector(:)
  
  stderr=0
  stdout=6

  ! parent file
  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_parent
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  
  ! grid node selector
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_grid_node_selector
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  ! grid cell selector
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_grid_cell_selector
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  
  ! subgrid node selector
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_subgrid_node_selector
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  ! subgrid cell selector
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_subgrid_cell_selector
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_selected_subgrid_parent
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  !
  ! read node-cell selector for grid and subgrid
  !
  call file_grid_node_selector%init(stderr,etb(path_grid_node_selector),13,'in')
  call read_selector(stderr, file_grid_node_selector, &
       selected_grid_nnode, original_grid_nnode, grid_node_selector)
  call file_grid_node_selector%kill(stderr)
  write(*,*) 'debug',etb(path_grid_node_selector)
  !
  call file_grid_cell_selector%init(stderr,etb(path_grid_cell_selector),13,'in')
  call read_selector(stderr, file_grid_cell_selector, &
       selected_grid_ncell, original_grid_ncell, grid_cell_selector)
  call file_grid_cell_selector%kill(stderr)
  write(*,*) 'debug',etb(path_grid_cell_selector)
  !
  call file_subgrid_node_selector%init(stderr,etb(path_subgrid_node_selector),13,'in')
  call read_selector(stderr, file_subgrid_node_selector, &
       selected_subgrid_nnode, original_subgrid_nnode, subgrid_node_selector)
  call file_subgrid_node_selector%kill(stderr)
  write(*,*) 'debug',etb(path_subgrid_node_selector)

  !
  call file_subgrid_cell_selector%init(stderr,etb(path_subgrid_cell_selector),13,'in')
  call read_selector(stderr, file_subgrid_cell_selector, &
       selected_subgrid_ncell, original_subgrid_ncell, subgrid_cell_selector)
  call file_subgrid_cell_selector%kill(stderr)
  write(*,*) 'debug',etb(path_subgrid_cell_selector)



  ! read  parent
  call file_parent%init(stderr, etb(path_parent),10,'in')
  read(file_parent%fn,*) i, original_subgrid_nnode
  read(file_parent%fn,*) j, original_subgrid_ncell
  ! set ncell and nnode of subgrid
  subgrid%nnode = original_subgrid_nnode
  subgrid%ncell = original_subgrid_ncell
  call file_parent%kill(stderr)

  call file_parent%init(stderr, etb(path_parent),10,'in')
  call subgrid%read_parent(stderr,file_parent)
  call file_parent%init(stderr, etb(path_parent),10,'in')


  !
  ! compute inverse node
  !
  allocate(inv_grid_node_selector(original_grid_nnode),stat=res)
  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
       ' work array inv_node_selector ',res)
  inv_grid_node_selector = 0
  do i = 1, selected_grid_nnode
     j = grid_node_selector(i)
     inv_grid_node_selector(j)  = i 
  end do

  !
  ! compute inverse cell
  !
  allocate(inv_grid_cell_selector(original_grid_ncell),stat=res)
  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
       ' work array inv_node_selector ',res)
  inv_grid_cell_selector = 0
  do i = 1, selected_grid_ncell
     j = grid_cell_selector(i)
     inv_grid_cell_selector( j ) = i 
  end do

  !
  ! compute new parent 
  !
  selected_subgrid%grid_level = 1
  selected_subgrid%nnode = selected_subgrid_nnode
  selected_subgrid%ncell = selected_subgrid_ncell
  selected_subgrid%nnode_parent = selected_grid_nnode
  selected_subgrid%ncell_parent = selected_grid_ncell

  allocate(selected_subgrid%cell_parent(selected_subgrid_ncell),stat=res)
  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
       ' work array subgrid_selected%cell_parent ',res)
  do icell_selected = 1, selected_subgrid%ncell
     icell_original    = subgrid_cell_selector(icell_selected)
     ifather_original  = subgrid%cell_parent(icell_original)
     selected_subgrid%cell_parent(icell_selected) = inv_grid_cell_selector(ifather_original)
  end do

  allocate(selected_subgrid%node_parent(2,selected_subgrid_nnode),stat=res)
  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
       ' work array subgrid_selected%node_parent ',res)
  do inode_selected = 1, selected_subgrid%nnode
     inode_original = subgrid_node_selector(inode_selected)
     n1_father_original  = subgrid%node_parent(1,inode_original)
     n2_father_original  = subgrid%node_parent(2,inode_original)
     selected_subgrid%node_parent(1,inode_selected) = inv_grid_node_selector(n1_father_original)
     selected_subgrid%node_parent(2,inode_selected) = inv_grid_node_selector(n2_father_original)
  end do

  !
  ! write to file
  !
  call file_selected_subgrid_parent%init(stderr, etb(path_selected_subgrid_parent),20,'out')
  call selected_subgrid%write_parent(stderr, file_selected_subgrid_parent)
  call file_selected_subgrid_parent%kill(stderr)


contains
  subroutine read_selector(lun_err, &
     file_selector,&
     nselected, noriginal,&
     selection)
  use Globals
  implicit none
  integer, intent(in) :: lun_err
  type(file) , intent(in) :: file_selector
  integer, intent(inout) :: nselected
  integer, intent(inout) :: noriginal
  integer, intent(inout), allocatable :: selection(:)
  ! local
  logical :: rc
  integer :: res
  integer :: lun
  integer :: i
  character(len=256) :: fname

  lun   = file_selector%lun
  fname = file_selector%fn
  read(lun,*,iostat=res) nselected, noriginal
  if (res.ne.0) rc = IOerr(lun_err, err_read , 'read_selector', &
       etb(fname),res)
  allocate(selection(nselected),stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_alloc , 'read_selector', &
       ' work array selection ',res)
  do i=1,nselected
     read(lun,*,iostat=res) selection(i)
     if (res.ne.0) then
        rc = IOerr(lun_err, err_read , 'read_selector', &
             etb(fname)//' selection array ',res)
     end if
  end do


end subroutine read_selector


 

end PROGRAM selection_parent

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./selection_parent.out <parent> <nodeselector> <cellselector> <parent_selected > <subgrid_nodeselector> <subgrid_cellselector>'
  write(lun_err,*) ' where '
  write(lun_err,*) ' parent               (in ) : grid-subgrid relation'
  write(lun_err,*) ' nodeselector         (in ) : grid node selector '
  write(lun_err,*) ' cellselector         (in ) : grid cell selector '
  write(lun_err,*) ' parent_selected      (in ) : selected grid - selected subgrid relation'
  write(lun_err,*) ' subgrid_nodeselector (out) : subgrid node selector '
  write(lun_err,*) ' subgrid_cellselector (out) : subgrid cell selector '

  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle


  
     

