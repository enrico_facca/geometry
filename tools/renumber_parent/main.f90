!>---------------------------------------------------------------------
!> \author{Enrico Facca}
!>
!> DESCRIPTION: 
!> This program that given (INPUTS):
!> 1 - parent file
!> 2 - node selector for coaser grid
!> 3 - cell selector for coaser grid
!> return (OUTPUTS):
!> 1 - parent file   for selected subgrid
!> 2 - node selector for selected subgrid
!> 3 - cell selector for selected subgrid
!>
!> usage ./selection_grid.out "parent" "node_selector" "cell_selector" 
!>                            "parent_selected" "node_selector_subgrid" 
!>                             "cell_selector_subgrid " 
!>
!>
!> REVISION HISTORY:
!> yyyymmdd - 
!> 20190507 - Creation of program
!<---------------------------------------------------------------------

PROGRAM selection_parent 
  use Globals
  use AbstractGeometry
  use TimeInputs
  use vtkloc

  implicit none
  type(abs_simplex_mesh) :: subgrid
  type(abs_simplex_mesh) :: selected_subgrid
  type(file)     :: file_parent
  type(file)     :: file_permutation
  type(file)     :: file_parent_permuted
  
  integer :: stderr=0,stdout=6,res,res_read,narg
  integer :: i,j,k,m, icell, inode,itime,level
  integer :: icell_original,icell_selected, ifather_original
  integer :: inode_original,inode_selected
  integer :: n1_father_original,n2_father_original

  integer :: original_grid_nnode
  integer :: npermutation
  integer :: original_grid_ncell
  integer :: selected_grid_ncell
  integer :: original_subgrid_nnode
  integer :: selected_subgrid_nnode
  integer :: original_subgrid_ncell
  integer :: selected_subgrid_ncell



  character(len=10024) :: arg
  character(len=1024) :: path_parent
  character(len=1024) :: path_permutation
  character(len=1024) :: path_parent_permuted


  logical :: rc
  logical endfile,endreading,makeother
  integer info
  
  integer ,allocatable :: grid_node_selector(:)
  integer ,allocatable :: grid_cell_selector(:)
  integer ,allocatable :: subgrid_node_selector(:)
  integer ,allocatable :: subgrid_cell_selector(:)

  integer ,allocatable :: permutation(:)
  integer ,allocatable :: new_node_parent(:,:)
  integer ,allocatable :: new_cell_parent(:)


  integer ,allocatable :: inv_grid_node_selector(:), inv_grid_cell_selector(:)
  
  stderr=0
  stdout=6

  ! parent file
  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_parent
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  
  ! grid node selectorpath_permutation
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_permutation
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  ! grid cell selector
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_parent_permuted
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  
  ! read  parent
  write(*,*) etb(path_parent)
  call file_parent%init(stderr, etb(path_parent),10,'in')
  read(file_parent%lun,*) level ! number of refinement
  read(file_parent%lun,*) i, original_subgrid_nnode
  read(file_parent%lun,*) j, original_subgrid_ncell
  ! set ncell and nnode of subgrid
  subgrid%grid_level = level
  subgrid%nnode = original_subgrid_nnode
  subgrid%ncell = original_subgrid_ncell
  call file_parent%kill(stderr)

  write(*,*) subgrid%nnode,subgrid%ncell, subgrid%grid_level

  call file_parent%init(stderr, etb(path_parent),10,'in')
  call subgrid%read_parent(stderr,file_parent)
  call file_parent%init(stderr, etb(path_parent),10,'in')
  write(*,*) subgrid%nnode,subgrid%ncell, subgrid%grid_level


  !
  ! read node or cell permutation
  !
  call file_permutation%init(stderr,etb(path_permutation),13,'in')
  call read_selector(stderr, file_permutation, &
       npermutation, original_grid_nnode, permutation)
  call file_permutation%kill(stderr)
  write(*,*) npermutation

  

  !
  ! permute parent 
  !
  if ( npermutation .eq. subgrid%nnode) then
     allocate(new_node_parent(2,subgrid%nnode)) 
     do inode=1,subgrid%nnode
        do i=1,2
           new_node_parent(i,inode)=permutation(subgrid%node_parent(i,inode))
        end do
     end do
     subgrid%node_parent=new_node_parent
     deallocate(new_node_parent) 
  else if ( npermutation .eq. subgrid%ncell ) then
     allocate(new_cell_parent(subgrid%ncell)) 
     do icell=1,subgrid%nnode
        new_cell_parent(icell)=permutation(subgrid%cell_parent(icell))
     end do
     subgrid%cell_parent=new_cell_parent
     deallocate(new_cell_parent) 
  else
     rc = IOerr(stderr, err_inp , 'renumber_parent', &
         'Permutation dimension not compatible with parent file',res)
  end if
     


  


!!$  !
!!$  ! compute inverse node
!!$  !
!!$  allocate(inv_grid_node_selector(original_grid_nnode),stat=res)
!!$  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
!!$       ' work array inv_node_selector ',res)
!!$  inv_grid_node_selector = 0
!!$  do i = 1, npermutation
!!$     j = grid_node_selector(i)
!!$     inv_grid_node_selector(j)  = i 
!!$  end do
!!$
!!$  !
!!$  ! compute inverse cell
!!$  !
!!$  allocate(inv_grid_cell_selector(original_grid_ncell),stat=res)
!!$  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
!!$       ' work array inv_node_selector ',res)
!!$  inv_grid_cell_selector = 0
!!$  do i = 1, selected_grid_ncell
!!$     j = grid_cell_selector(i)
!!$     inv_grid_cell_selector( j ) = i 
!!$  end do
!!$
!!$  !
!!$  ! compute new parent 
!!$  !
!!$  selected_subgrid%grid_level = 1
!!$  selected_subgrid%nnode = selected_subgrid_nnode
!!$  selected_subgrid%ncell = selected_subgrid_ncell
!!$  selected_subgrid%nnode_parent = npermutation
!!$  selected_subgrid%ncell_parent = selected_grid_ncell
!!$
!!$  allocate(selected_subgrid%cell_parent(selected_subgrid_ncell),stat=res)
!!$  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
!!$       ' work array subgrid_selected%cell_parent ',res)
!!$  do icell_selected = 1, selected_subgrid%ncell
!!$     icell_original    = subgrid_cell_selector(icell_selected)
!!$     ifather_original  = subgrid%cell_parent(icell_original)
!!$     selected_subgrid%cell_parent(icell_selected) = inv_grid_cell_selector(ifather_original)
!!$  end do
!!$
!!$  allocate(selected_subgrid%node_parent(2,selected_subgrid_nnode),stat=res)
!!$  if (res.ne.0) rc = IOerr(stderr, err_alloc , 'select_timedata', &
!!$       ' work array subgrid_selected%node_parent ',res)
!!$  do inode_selected = 1, selected_subgrid%nnode
!!$     inode_original = subgrid_node_selector(inode_selected)
!!$     n1_father_original  = subgrid%node_parent(1,inode_original)
!!$     n2_father_original  = subgrid%node_parent(2,inode_original)
!!$     selected_subgrid%node_parent(1,inode_selected) = inv_grid_node_selector(n1_father_original)
!!$     selected_subgrid%node_parent(2,inode_selected) = inv_grid_node_selector(n2_father_original)
!!$  end do

  !
  ! write to file
  !
  call file_parent_permuted%init(stderr, etb(path_parent_permuted),20,'out')
  write(*,*) subgrid%nnode,subgrid%ncell,level
  call subgrid%write_parent(stderr, file_parent_permuted)
  call file_parent_permuted%kill(stderr)


contains
  subroutine read_selector(lun_err, &
     file_selector,&
     nselected, noriginal,&
     selection)
  use Globals
  implicit none
  integer, intent(in) :: lun_err
  type(file) , intent(in) :: file_selector
  integer, intent(inout) :: nselected
  integer, intent(inout) :: noriginal
  integer, intent(inout), allocatable :: selection(:)
  ! local
  logical :: rc
  integer :: res
  integer :: lun
  integer :: i
  character(len=256) :: fname

  lun   = file_selector%lun
  fname = file_selector%fn
  read(lun,*,iostat=res) nselected, noriginal
  if (res.ne.0) rc = IOerr(lun_err, err_read , 'read_selector', &
       etb(fname),res)
  allocate(selection(nselected),stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_alloc , 'read_selector', &
       ' work array selection ',res)
  do i=1,nselected
     read(lun,*,iostat=res) selection(i)
     if (res.ne.0) then
        rc = IOerr(lun_err, err_read , 'read_selector', &
             etb(fname)//' selection array ',res)
     end if
  end do


end subroutine read_selector


 

end PROGRAM selection_parent

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./selection_parent.out <parent> <nodeselector> <cellselector> <parent_selected > <subgrid_nodeselector> <subgrid_cellselector>'
  write(lun_err,*) ' where '
  write(lun_err,*) ' parent          (in ) : grid-subgrid relation'
  write(lun_err,*) ' permutation     (in ) : node or cell permutation'
  write(lun_err,*) ' parent_permuted (out) : renumbered grid - subgrid relation'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle


  
     

