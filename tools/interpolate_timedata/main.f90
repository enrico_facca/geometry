!>---------------------------------------------------------------------
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> Program to interpolate timedata defined on cell or on nodes
!> from a coaser triangulation (2d, 3d, surface) to timedata
!> defined on a finer triangulation.
!>
!> USAGE:
!>
!> ./interpolate_timedata.out data <subgrid> <parent data_inter 
!>
!> - parent     : coarser-finer grids realtions     
!> - data       : data defined an a coaser grid
!> - data_inter : ouput data interpolated
!> 
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM interpolate_timedata
  use Globals
  use AbstractGeometry
  use TimeInputs
  use TimeOutputs
  
  implicit none
  
  type(abs_simplex_mesh) :: subgrid

  integer  :: dim,ndata,ndataref
  logical  :: rc
  integer  :: res,n1,n2
  character(len=256) :: inputs
  character(len=256) :: help_passed
  character(len=256) :: data_fname
  character(len=256) :: subgrid_fname
  character(len=256) :: parent_fname
  character(len=256) :: dataref_fname
  character(len=256) :: option

  type(file) :: fdata
  type(file) :: fsubgrid
  type(file) :: fparent
  type(file) :: fdataref

  type(TimeData) :: tdata



  integer :: stderr,stdout,debug,count !,res
  integer :: i, j,icell,inode,inode_parent
  real(kind=double) :: time,imbalance,tzero,dnrm2
  real(kind=double), allocatable :: v1(:), v2(:)
  type(TDout) :: dataref

  logical :: steady,endfile
  logical :: end_reached
  logical :: end_files
  logical :: end_reached_source
  logical :: end_reached_sink  
  logical :: end_reached_dirac_source 
  logical :: end_reached_dirac_sink   


  stderr=0
  stdout=6


  !>----------------------------------------------------------------------------
  count = command_argument_count()
  if ( count .eq. 1) then
     call get_command_argument(1,inputs, status=res)
     help_passed=etb(inputs)
     if ( help_passed .eq. '--help') then
        call print_help_message(stdout)
        write(stdout,*) 'DESCRIPTION:' 
        write(stdout,*) ' Program to interpolate timedata defined on cell or on nodes'
        write(stdout,*) ' from a coaser triangulation (2d, 3d, surface) to timedata'
        write(stdout,*) ' defined on a finer triangulation'
        stop
     else
        call print_help_message(stderr)
     end if
  end if

           


  !>----------------------------------------------------------------------------
  !> Geometry info
  !> Read original grid
  ! no renumbering
  ! no connection of second level   
  call get_command_argument(1,parent_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument parent wrong res= ',res)
  end if

  call get_command_argument(2,data_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument data wrong res= ',res)
  end if
  
  call get_command_argument(3,dataref_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument data interpolated wrong res= ',res)
  end if

  if (count .eq. 4) then
     call get_command_argument(4,option, status=res)
     if ( res.ne.0) then
        call print_help_message(stderr)
        rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
             '  argument data interpolated wrong res= ',res)
     end if
  end if
  
  call fdata%init(stderr,etb(data_fname),10,'in')
  !call fsubgrid%init(stderr,etb(subgrid_fname),11,'in')
  call fparent%init(stderr,etb(parent_fname),12,'in')
  call fdataref%init(stderr,etb(dataref_fname),13,'out')
  

  ! init subgrid
  !call subgrid%read_mesh(stderr,fsubgrid)
  call subgrid%read_parent(stderr,fparent)
  

  call tdata%init(stderr, fdata)
  write(*,*)tdata%dimdata
  
  if ( tdata%ndata .eq. subgrid%ncell_parent ) then
     !
     ! data_type = 'cell'
     !
     ndataref = subgrid%ncell
  else if ( tdata%ndata .eq. subgrid%nnode_parent ) then
     !
     ! data_type = 'nnode'
     !
     ndataref = subgrid%nnode
  else
     write(0,*) 'wrong dimensions'
     stop
  end if

  call dataref%init(stderr, tdata%dimdata,ndataref)
  open(fdataref%lun, file = fdataref%fn)
  write(fdataref%lun,*)  dataref%dimdata,  dataref%ndata

  if ( (count .eq. 4) .and. (option .eq. 'dirichlet') ) then
     allocate(&
          v1(tdata%dimdata),&
          v2(tdata%dimdata),&
          stat=res)
     if (res .ne. 0)& 
          rc = IOerr(stderr, err_alloc , 'interpolate_timedata', &
          ' work arrays v1,v2 res= ',res)
  end if
     
  ! build
  endfile=.false.
  steady=.false.

  
  time = tdata%TDtime(1)
  do while ( (.not. steady ) .and. ( .not. endfile  ) )
     call tdata%set(stderr,fdata,time,endfile)
     dataref%time = time
     if ( ndataref .eq. subgrid%ncell ) then
        
        do i=1,tdata%dimdata
           call subgrid%proj_subgrid(tdata%TDactual(i,:),dataref%TDactual(i,:))
        end do
     end if
     if ( ndataref .eq. subgrid%nnode ) then
        if ( (count .eq. 4) .and. (option .eq. 'dirac')) then
           do inode=1,subgrid%nnode
              if ( subgrid%node_parent(1,inode) .eq. &
                   subgrid%node_parent(2,inode) ) then
                 inode_parent = subgrid%node_parent(1,inode)
                 do i=1,tdata%dimdata
                    dataref%TDactual(i,inode) =  tdata%TDactual(i,inode_parent)
                 end do
              end if
           end do
        else if ( (count .eq. 4) .and. (option .eq. 'dirichlet')) then
           do inode=1,subgrid%nnode
              n1 = subgrid%node_parent(1,inode)
              n2 = subgrid%node_parent(2,inode)
              v1 = tdata%TDactual(:,n1)
              v2 = tdata%TDactual(:,n2)
              if (dnrm2(tdata%dimdata,v1,1)>zero .and. dnrm2(tdata%dimdata,v2,1)>zero ) then
                 do i=1,tdata%dimdata
                    dataref%TDactual(i,inode) =  onehalf*(v1(i)+v2(i))
                 end do
              end if
           end do
        else
           do i=1,tdata%dimdata
              call subgrid%projnode_subgrid(tdata%TDactual(i,:),dataref%TDactual(i,:))
           end do
        end if
     end if
     call dataref%write2dat(fdataref%lun)
     time =  tdata%TDtime(2)
     if ( time .eq. huge) then
        steady = .True.
        write(fdataref%lun,*) 'time ', huge
     end if
  end do

  call fdata%kill(stderr)
  call fsubgrid%kill(stderr)
  call fparent%kill(stderr)
  call fdataref%kill(stderr)

  if ( (count .eq. 5) .and. (option .eq. 'dirichlet') ) then
     deallocate(&
          v1,&
          v2,&
          stat=res)
     if (res .ne. 0)& 
          rc = IOerr(stderr, err_dealloc , 'interpolate_timedata', &
          ' work arrays v1,v2 res= ',res)
  end if

  
  contains
    subroutine print_help_message(lun)
      implicit none
      integer, intent(in) :: lun
      !local
      character(len=256) :: msg
      msg=' USAGE: " ./interpolate_timedata.out data parent data_inter <option>"'
      write(lun,*) etb(msg )
      msg=' - parent     : coarser-finer grids realtions'     
      write(lun,*) etb(msg )
      msg=' - data       : data defined an a coaser grid'
      write(lun,*) etb(msg )
      msg=' - data_inter : ouput data interpolated on finer grid'
      write(lun,*) etb(msg )
      msg=' - option     : Three options for interpolation (default-dirac-dirichlet)'
      write(lun,*) etb(msg )
      msg=' -   "default"  . Linear interpolation, used if no option is passed"'
      write(lun,*) etb(msg )
      msg=' -   "dirac"    . Copy the non-zeros only to nodes that belong'
      write(lun,*) etb(msg )
      msg=' -                to the original the coaser grid'
      write(lun,*) etb(msg )
      msg=' -   "dirichlet". Interpolate on edges'
      write(lun,*) etb(msg )
      msg=' or  : " ./interpolate_timedata.out --help " for help and description '
      write(lun,*) etb(msg )

    end subroutine print_help_message

  

end PROGRAM interpolate_timedata
