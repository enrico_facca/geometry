!>---------------------------------------------------------------------
!> \author{Enrico Facca}
!>
!> DESCRIPTION: 
!> This program compute the lp norm of timedata 
!> (repository globals) associated to grids
!> 
!> usage ./normp.out "path_grid" "path_timedata_file" power
!>
!> path_grid      :: path of the grid(2d, 3d, or surface)
!> path_timedata  :: path of timedata of dimension.
!>                   Only ncell or nnode is accepted
!> lp_norm        :: $lp_norm \eq  0$ =>$L^{\inty}$-norm=maxval
!>                   $lp_norm \geq 1$ =>$L^{p}$-norm 
!> Module Geometry2d is used for all tipo of Triangularization 
!> 2d, 3d and surface. 
!>
!> REVISION HISTORY:
!> yyyymmdd - 
!> 20190413 - Creation of program
!<---------------------------------------------------------------------

PROGRAM lpnorm
  use Globals
  use Geometry2d
  use TimeInputs
  use vtkloc

  implicit none
  type(mesh)     :: grid
  type(file)     :: file_grid, file_data
  type(TimeData) :: data_input

  integer :: stderr=0,stdout=6
  integer :: i,j,k, itria,itime
  integer :: ndata,dim
  real(kind=double) :: time

  character(len=256) :: arg
  character(len=256) :: path_grid
  character(len=256) :: path_data

  ! local vars
  integer :: res,ncoord,nnodeincell,nnode,ncell,icell,inode
  integer :: connection(5)
  logical :: rc
  character(len=256) :: rdwr,str,first_line,fname
  integer :: u_number


  logical endfile,endreading
  integer info

  real(kind=double) :: vec12(3),vec13(3),vec14(3),cross_prod(3)
  real(kind=double), allocatable :: node_size(:),cell_size(:),temp(:)
  real(kind=double) :: p_exponent, norm_out,dnrm2,ddot
  
  info=0
  
  CALL getarg(1, arg)
  read(arg,'(a256)',iostat=res) path_grid
  info=info+res
  
  CALL getarg(2, arg)
  read(arg,'(a256)',iostat=res) path_data
  info=info+res
  
  CALL getarg(3, arg)
  read(arg,*,iostat=res) p_exponent
  info=info+res

  if( info .ne. 0) rc = IOerr(6, err_inp , 'main', &
      ' Right usage : ./lp_norm.out grid file p_exponent   ')

  
  !> grid reading
  file_grid%fn=etb(path_grid)
  file_grid%lun=10
  open(file_grid%lun,file=file_grid%fn)
  
  !
  ! grid read "manually"
  !
  u_number    = file_grid%lun
  fname       = file_grid%fn
  

  read(u_number,*,iostat=res) grid%nnode
  if(res .ne. 0) rc = IOerr(6, err_inp , 'read_mesh', &
       etb(fname) // ' vars nnode',res)

  read(u_number,*,iostat=res) grid%ncell
  if(res .ne. 0) rc = IOerr(6, err_inp , 'read_mesh', &
       etb(fname) // ' vars ncell',res)

  allocate(grid%coord(3,grid%nnode),stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc, 'read_mesh', &
       '  type mesh member coord (array)',res)

  ! read first line to check if the the third coordinate is present 
  read(u_number,'(a)',iostat=res) first_line
  ncoord=3
  read(first_line,*,iostat=res) (grid%coord(k,1),k=1,ncoord)
  if (res .ne. 0) THEN
     grid%coord(3,:) = zero
     ! read only first two column, the first is initialized to zero
     ncoord = 2
     read(first_line,*,iostat=res) (grid%coord(k,1),k=1,ncoord)
     if(res .ne. 0) THEN
        write(rdwr,'(i5)') 1
        str=etb(rdwr)//'/'
        rc = IOerr(6, err_inp , 'read_mesh', &
             trim(etb(fname)) //&
             ' type mesh member array coord at line '//&
             trim(str),res)
     end if
  end if

  do j=2,grid%nnode
     read(u_number,*,iostat=res) (grid%coord(k,j),k=1,ncoord)
     if(res .ne. 0) THEN
        write(rdwr,'(i5)') j
        str=etb(rdwr)//'/'
        rc = IOerr(6, err_inp , 'read_mesh', &
             trim(etb(fname)) //&
             ' type mesh member array coord at line '//&
             trim(str),res)
     end if
  end do

  nnodeincell = 4
  read(u_number,'(a)',iostat=res) first_line
  !
  ! try to read 4 node + element material
  !
  read(first_line,*,iostat=res) (connection(k), k=1,nnodeincell+1)
  !
  ! if error then 2d or surface grid
  !
  if (res .eq. 0) THEN
     allocate(grid%topol(nnodeincell+1,grid%ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc, 'read_mesh', &
          '  type mesh member triangles (array)',res)
     grid%topol(:,1) = connection(1:nnodeincell+1)
  else
     nnodeincell = nnodeincell - 1
     allocate(grid%topol(nnodeincell+1,grid%ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc, 'read_mesh', &
          '  type mesh member triangles (array)',res)
     read(first_line,*,iostat=res) (connection(k), k=1,nnodeincell+1)
     if ( res .eq. 0 )then 
        grid%topol(:,1) = connection(1:nnodeincell+1)
     else
        ! graph case
        nnodeincell = nnodeincell - 1
        read(first_line,*,iostat=res) (connection(k), k=1,nnodeincell)
        grid%topol(1:nnodeincell,1) = connection(1:nnodeincell)
        grid%topol(nnodeincell+1,1) = j 
     end if
  end if
  grid%nnodeincell = nnodeincell


  do j=2,grid%ncell
     read(u_number,*,iostat=res) (grid%topol(k,j), k=1,nnodeincell)
     grid%topol(nnodeincell+1,j) = j
     if(res .ne. 0) then
        write(rdwr,'(i5)') j
        str=etb(rdwr)//'/'
        write(rdwr,'(i5)') k
        str=trim(str)//etb(rdwr)
        rc = IOerr(6, err_inp , 'read_mesh', &
             trim(etb(fname)) // &
             ' type mesh member array topol at line/col '//trim(str),res)
     end if
  end do
  close (file_grid%lun)

  allocate(cell_size(grid%ncell), node_size(grid%ncell),stat=res)
  if ( res .ne. 0) rc = IOerr(stderr, err_inp , 'main', &
       'work array cell_size node_size')

  cell_size=zero
  node_size=zero
  if (grid%nnodeincell .eq. 3) then
     do icell=1,grid%ncell
        vec12 = grid%coord(:,grid%topol(1,icell))-grid%coord(:,grid%topol(2,icell))
        vec13 = grid%coord(:,grid%topol(1,icell))-grid%coord(:,grid%topol(3,icell))
        cross_prod = cross(vec12,vec13)
        cell_size(icell) = onehalf*dnrm2(3,cross_prod,1)
        do j=1,3
           inode=grid%topol(j,icell)
           node_size(inode)=node_size(inode)+cell_size(icell)*onethird
        end do
     end do
  else
     do icell=1,grid%ncell
        vec12 = grid%coord(:,grid%topol(1,icell))-grid%coord(:,grid%topol(2,icell))
        vec13 = grid%coord(:,grid%topol(1,icell))-grid%coord(:,grid%topol(3,icell))
        vec14 = grid%coord(:,grid%topol(1,icell))-grid%coord(:,grid%topol(4,icell))
        cross_prod = cross(vec12,vec13)
        cell_size(icell) = onesixth*ddot(3,vec14,1,cross_prod,1)
        do j=1,4
           inode=grid%topol(j,icell)
           node_size(inode)=node_size(inode)+cell_size(icell)*onefourth
        end do
     end do
  end if
     


  !
  ! init data
  !
  call file_data%init(stderr,etb(path_data),11,'in')
  call data_input%init(stderr,file_data)
  
     
  !
  ! check consistency
  !
  if (.not.( ( data_input%ndata .eq. grid%nnode)   .or. &
       (data_input%ndata .eq. grid%ncell) ) ) then    
     rc = IOerr(stderr, err_inp , 'main', &
          'files '//etb(file_data%fn)//&
          'is not compatible with  grid'//&
          etb(file_grid%fn))

  end if

  dim=data_input%dimdata
  ndata=data_input%Ndata
  allocate( temp(ndata),stat=res)
  if ( res .ne. 0) rc = IOerr(stderr, err_alloc , 'main', &
       'work array difference, vector_difference')
 
  endfile=.false.
  endreading=.false.
  time  = data_input%TDtime(1)
  itime = 0
  do while ( .not. endreading )
     call data_input%set(stderr, file_data, time, endfile)
     if (dim .eq. 1 ) then
        temp(:)=data_input%TDactual(1,:)
     else
        do i=1,ndata
           temp(i)=dnrm2(dim,data_input%TDactual(:,i),1)
        end do
     end if

     if ( ndata == grid%nnode ) then
        norm_out=normp(ndata, p_exponent, temp, node_size)
     else
        norm_out=normp(ndata, p_exponent, temp, cell_size)
     end if

     write(stdout,'(2e16.8)') time, norm_out
          
     !
     ! if both file are in steady state 
     !
     if ( data_input%steadyTD ) then
        endreading=.True.
     end if

     !
     ! if one file reached the there is no time exit
     !
     if ( endfile ) then
        endreading=.True.
     end if


     !
     ! next time
     ! 
     time = data_input%TDtime(2)
   end do
  

   close(file_data%lun)


contains
  !>------------------------------------------------------------------
  !> Evaluates the p-norm using midpoint rules
  !>------------------------------------------------------------------
  function normp(ndata,power,data,weight) result(total)
    use Globals
    implicit none
    integer,           intent(in) :: ndata
    real(kind=double), intent(in) :: power
    real(kind=double), intent(in) :: data(ndata)
    real(kind=double), intent(in) :: weight(ndata)
    real(kind=double) :: total
    !local 
    integer :: i
    real(kind=double) :: ddot

    if ( abs(power) < small) then
       total = maxval( abs( data ) )
    else if ( abs(power-one) < small ) then
       total = ddot(ndata, abs(data) ,1, weight,1)
    else
       total = zero
       do i = 1, ndata
          total = total + abs(data(i)) ** power * weight(i)
       end do
       total = total ** ( one / power )
    end if
  end function normp

end PROGRAM lpnorm
