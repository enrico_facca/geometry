!>---------------------------------------------------------------------
!> 
!> \author{Enrico Facca and Mario Putti} 
!> 
!> DESCRIPTION:
!> Program for the projection of timedata data defined on
!> grid nodes (p1 varaible) to a timedata defined of cells
!> (p0 variable). New data are defined as:
!> $\data_cell(icell) = \int_{icell} var_node / |Cell|
!> 
!> INPUTS:  
!> - grid (in)  :: general_triangulation
!> - data_node  :: timedata on nodes of grid
!>
!> OUTPUTS: 
!> - data cell  :: timedata on cell grid defined as
!>   data_cell(icell) = sum_{i=1,nnodeincell} time_data(grid%topol(i)) / |nnodeincell| 
!> 
!> Example triangular grid on 2d:
!>
!> example_grid 
!> 3 
!> 1 3
!> 0.0 0.0 
!> 1.0 0.0 
!> 0.0 1.0 
!> 1 2 3 1
!>
!> example_p1data
!> 1 3
!> time 0.0
!> 3
!> 1 1.0
!> 2 2.0
!> 3 3.0
!> time 1e30

!> example_p0data
!> 1 1
!> time 0.0
!> 1
!> 1 2.0
!> time 1e30


!> REVISION HISTORY:
!> 14 Aug 2019 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM project_p1top0
  use Globals
  use AbstractGeometry
  use TimeInputs
  use TimeOutputs
  
  implicit none
  
  type(abs_simplex_mesh) :: grid

  integer  :: dim,ndata,ndata_cell
  logical  :: rc
  integer  :: res, narg
  character(len=256) :: inputs
  character(len=256) :: data_fname
  character(len=256) :: path_grid
  character(len=256) :: path_data_node
  character(len=256) :: path_data_cell

  type(file) :: fgrid
  type(file) :: fdata_node
  type(file) :: fdata_cell

  type(TimeData) :: tdata_node
  type(TDout) :: tdata_cell


  integer :: stderr,stdout
  integer :: icell,inode
  real(kind=double) :: time,tzero


  logical :: steady,endfile
  logical :: end_reached
  logical :: end_files


  stderr=0
  stdout=6

  !
  ! read arguments
  ! 
  narg=1
  call  get_command_argument(narg, path_grid,status=res) 
  if (res .ne. 0) call err_handle(stderr,narg)
   
  narg=narg+1   
  call  get_command_argument(narg, path_data_node,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
  narg=narg+1
  call  get_command_argument(narg, path_data_cell,status=res)
  if ( res .ne. 0) call err_handle(stderr,narg)

  !
  ! read grid
  !
  call fgrid%init(stderr, etb(path_grid),20,'in')
  call grid%read_mesh(stderr,fgrid)
  call fgrid%kill(stderr)

  !
  ! init file and check dimension
  !
  call fdata_node%init(stderr, etb(path_data_node),21,'in')
  call tdata_node%init(stderr, fdata_node)
  if ( tdata_cell%ndata .ne. grid%nnode )  then
     if(res .ne. 0) rc = IOerr(stderr, err_inp,&
          'main', &
          ' time data is not defined on grid nodes')
  end if

  !
  ! project and write data
  !
  endfile=.false.
  steady=.false.

  call fdata_cell%init(stderr, etb(path_data_cell),22,'out')
  call tdata_cell%init(stderr, tdata_node%dimdata, grid%ncell)
  write(fdata_cell%lun,*)  tdata_cell%dimdata,  tdata_cell%ndata
  
  time = tdata_node%TDtime(1)
  do while ( (.not. steady ) .and. ( .not. endfile  ) )
     !
     ! read data
     !
     call tdata_node%set(stderr,fdata_node,time,endfile)
     
     !
     ! evaluate and write data
     !
     tdata_cell%time = time
     call p1top0(tdata_node%dimdata, grid,&
          tdata_node%TDactual, tdata_cell%TDactual)
     call tdata_cell%write2dat(fdata_cell%lun)

     !
     ! next time
     !
     time =  tdata_node%TDtime(2)
     if ( time .eq. huge) then
        steady = .True.
        write(fdata_cell%lun,*) 'time ', huge
     end if
  end do

  !
  ! free memory
  !
  
  call fdata_node%kill(stderr)
  call fdata_cell%kill(stderr)
    
end PROGRAM project_p1top0

subroutine p1top0(ndata,grid,data_node,data_cell)
  use Globals
  use AbstractGeometry
  implicit none
  integer,                intent(in   ) :: ndata
  type(abs_simplex_mesh), intent(in   ) :: grid
  real(kind=double),      intent(in   ) :: data_node(ndata,grid%nnode)
  real(kind=double),      intent(inout) :: data_cell(ndata,grid%ncell)
  ! local
  integer :: icell,iloc,inode, idata
  real(kind=double) :: factor

  factor=one/grid%nnodeincell
  data_cell = zero
  
  do icell= 1, grid%ncell
     do iloc = 1, grid%nnodeincell
        inode=grid%topol(iloc,icell)
        do idata = 1,ndata
           data_cell(idata,icell) = data_cell(idata,icell) + &
                data_node(ndata,inode)*factor
        end do
     end do
  end do

  
end subroutine p1top0

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  
  
  write(lun_err,*) ' Error passing arguments, correct usage: '
  write(lun_err,*) ' ./renumber_grid.out <grid> <grid_renumbered> [<parent> <parent_renumbered>]'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid     (in ) : triangulation in ascii'
  write(lun_err,*) ' p1data   (in ) : timedata on grid nodes'
  write(lun_err,*) ' p0data   (out) : timedata on grid cells, projection of p1data'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Error reading argument number : ', narg)
end subroutine err_handle

  
  


