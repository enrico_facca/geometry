!>---------------------------------------------------------------------
!> \author{Enrico Facca}
!>
!> DESCRIPTION: 
!> This program that given (INPUTS):
!> 1 - graph G  (may be a general unstructured grid) 
!> 2 - a steady scalar timedata var of dimension 
!>     equal to ncell or nnode
!> 3 - lower and upper bound (min,max)
!> return (OUTPUTS):
!> 1- a graph G' containg all nodes (cells) such that
!>    $var(inode)\in [min,max]$ ($var(icell)\in [min,max]$)
!> 2- A list of integer containg the realtion of the grids.
!>    Usefull to convert data defined on graph G into dato for G'
!>    nnode_son nnode_father ( ncell_son ncell_father)
!>    index of node (cell) in original grid of node (cell) 1 
!>    index of node (cell) in original grid of node (cell) 2
!>    ....
!>    index of node (cell) in original grid of node (cell) 1 
!>
!> usage ./selection_grid.out "graph" "data" min max "subgraph" "selector"
!>
!>
!> REVISION HISTORY:
!> yyyymmdd - 
!> 20190507 - Creation of program
!<---------------------------------------------------------------------

PROGRAM selection_grid 
  use Globals
  use AbstractGeometry
  use TimeInputs
  use vtkloc

  implicit none
  type(abs_simplex_mesh) :: grid
  type(abs_simplex_mesh) :: filtered_grid
  type(file)     :: file_grid
  type(file)     :: file_data
  type(file)     :: file_gridout
  type(file)     :: file_relation
  type(file)     :: file_relation_other
  type(TimeData) :: data

  integer :: stderr=0,stdout=6,res,res_read,narg
  integer :: i,j,k,m, icell, inode,itime
  integer :: ndata,dimdata,nother
  real(kind=double) :: time,lower_bound, upper_bound

  character(len=256) :: arg
  character(len=256) :: path_grid
  character(len=256) :: path_data
  character(len=256) :: path_gridout
  character(len=256) :: path_relation
  character(len=256) :: path_relation_other


  logical :: rc
  logical endfile,endreading,makeother
  integer info

  logical,allocatable :: selector(:), other_selector(:)
  
  stderr=0
  stdout=6


  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_grid
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_data
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,*,iostat=res_read) lower_bound
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,*,iostat=res_read) upper_bound
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_gridout
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_relation
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  makeother = .False.
  if (res .eq. 0) then
     read(arg,'(a256)',iostat=res_read) path_relation_other
     if (res_read .ne. 0) call err_handle(stderr,narg)
     makeother = .True.
  end if


  ! init mesh and input data
  call file_grid%init(stderr, etb(path_grid),10,'in')
  call grid%read_mesh(stderr,file_grid)
  call file_grid%kill(stderr)


  call file_data%init(stderr, etb(path_data),11,'in')
  call data%init(stderr,file_data)
  call data%set(stderr,file_data,data%TDtime(1),rc)
  call file_data%kill(stderr)
  ndata=data%ndata
  
  ! check data consistency
  if ( .not. ( &
       ( data%ndata .eq. grid%ncell)  .or.&
       ( data%ndata .eq. grid%nnode)  ) ) then
     rc = IOerr(stderr, err_inp , 'main', &
          ' Data passed not consitent with grid')
  end if
  
     
  ! only scalar so far
  if (  data%dimdata .ne. 1) then
     rc = IOerr(stderr, err_inp , 'main', &
          ' Data passed is not scalar')
  end if
  ! only steady state
  if ( .not. data%steadyTD ) then
     rc = IOerr(stderr, err_inp , 'main', &
          ' Data passed is not steady steady state')
  end if
  ! only steady state
  if ( upper_bound .lt. lower_bound ) then
     rc = IOerr(stderr, err_inp , 'main', &
          ' upper_bound is less than lower_bound  ')
  end if
  
  if ( ( data%ndata .eq. grid%ncell) ) nother=grid%nnode
  if ( ( data%ndata .eq. grid%nnode) ) nother=grid%ncell
  
  
  allocate(&
       selector(ndata),&
       other_selector(nother),&
       stat=res)
  if (res .ne. 0) rc = IOerr(stderr, err_alloc , 'main', &
          ' vars selector other_selector')
  
  do i=1,data%ndata
     selector(i) = ( & 
          ( data%TDActual(1,i) .ge. lower_bound ) .and. &
          ( data%TDActual(1,i) .le. upper_bound )  )
  end do


  
  call filtered_grid%init_selection(stderr, &
       grid, data%ndata, selector, other_selector)
  call file_gridout%init(stderr, etb(path_gridout),12,'out')
  call filtered_grid%write_mesh(stderr,file_gridout)
  call file_gridout%kill(stderr)

  !
  ! open output files end write 
  !
  call file_relation%init(stderr, etb(path_relation),14,'out')  
  !
  ! write head
  !
  if ( data%ndata .eq. grid%nnode) then
     write(file_relation%lun,*,iostat=res) filtered_grid%nnode, grid%nnode, &
       ' ! # of filtered nodes, # number of original nodes '
  else
     write(file_relation%lun,*,iostat=res) filtered_grid%ncell, grid%ncell,&
          ' ! # of filtred cell, # of original cell '
  end if
  !
  ! write relation
  !
  if ( ndata .eq. grid%nnode) then
     m=0
     do inode = 1, grid%nnode
        if ( selector(inode) ) then
           write(file_relation%lun,*,iostat=res)  inode
        end if
     end do
  else
     do icell = 1, grid%ncell
        if ( selector(icell) ) then
           write(file_relation%lun,*,iostat=res)  icell
        end if
     end do
  end if
  call file_relation%kill(stderr)

  !
  ! Write other relation
  !
  if ( makeother ) then
     !
     ! Write head
     !
     call file_relation_other%init(stderr, etb(path_relation_other),15,'out')  
     if ( data%ndata .ne. grid%nnode) then
        write(file_relation_other%lun,*,iostat=res) filtered_grid%nnode, grid%nnode, &
             ' ! # of filtered nodes, # number of original nodes '
     else
        write(file_relation_other%lun,*,iostat=res) filtered_grid%ncell, grid%ncell,&
             ' ! # of filtred cell, # of original cell '
     end if
     !
     ! Write relation
     !
     if ( ndata .ne. grid%nnode) then
        m=0
        do inode = 1, grid%nnode
           if ( other_selector(inode) ) then
              write(file_relation_other%lun,*,iostat=res)  inode
           end if
        end do
     else
        do icell = 1, grid%ncell
           if ( other_selector(icell) ) then
              write(file_relation_other%lun,*,iostat=res)  icell
           end if
        end do
     end if
       call file_relation_other%kill(stderr)
  end if
 

end PROGRAM selection_grid

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./selection_grid.out <grid> <data> <min> <max> <grid_out> <selector> [<other_selector>]'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid     (in     ) : triangulation in ascii'
  write(lun_err,*) ' data     (in     ) : timedata used for selection defined on cell or nodes'
  write(lun_err,*) ' min      (in     ) : lower bound for thresholding'
  write(lun_err,*) ' max      (in     ) : upper bound for thresholding'
  write(lun_err,*) ' grid_out (out    ) : triangulation in ascii after selection'
  write(lun_err,*) ' selector (out    ) : grid/grid_out relation for data given (cell/node)'
  write(lun_err,*) ' other    (otp,out) : grid/grid_out relation for other data (node/cell)'


  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle

