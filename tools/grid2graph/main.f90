!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> This Program extracts the graph of the cell-to-cell connection of
!> of a 2d grid
!> 
!>
!> REVISION HISTORY:
!> dd mm  yyyy - 
!> 03 May 2019 - First version
!>
!> TODO_dd_mmm_yyyy - (TODO_name ) :TODO_describe_appropriate_changes 
!<---------------------------------------------------------------------

PROGRAM grid2graph
  use Globals
  use AbstractGeometry

  implicit none
  type(abs_simplex_mesh) :: grid,graph
  type(file):: file_grid, file_graph
  type(abs_simplex_mesh) :: node_graph

  integer :: stderr,stdout,lun
  integer :: i,j

  character(len=256) :: arg
  character(len=256) :: path_grid
  character(len=256) :: path_graph
  character(len=256) :: option
  logical :: rc
  character(len=256) :: rdwr,str,first_line
  integer, allocatable:: topol(:,:)

  logical endfile
  integer ppos,pbar,nargs,narg,res

  character(len=8) :: label

  stderr = 0

  ! Geometry info
  ! Read original grid
  ! no renumbering
  ! no connection of second level
  nargs=command_argument_count()
  if ( nargs < 3 ) call err_handle(stderr,1)
  
  narg=1
  call  get_command_argument(narg, path_grid,status=res) 
  if (res .ne. 0) call err_handle(stderr,narg)

  narg=narg+1
  call  get_command_argument(narg, path_graph,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
   
  narg=narg+1   
  call  get_command_argument(narg, option,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  if ( .not. ( ( etb(option) .eq. 'node') .or. &
       ( etb(option) .eq. 'cell' ) ) ) then
     call err_handle(stderr,narg)
  end if
  

  ! read grid
  call file_grid%init(stderr, etb(path_grid),10,'in')
  call grid%read_mesh(stderr, file_grid) 
  call file_grid%kill(stderr)

  select case ( etb(option) )
  case ('node')
     !
     ! buidl the graph of node-to-node connection
     !
     call grid%build_edge_connection(stderr)
     call graph%init_from_data(stderr, &
          grid%nnode,grid%nedge, 2,'edge',&
          grid%edge_iside, grid%coord)
  case ('cell')
     !
     ! build cell-to-cell connection graph
     ! only triangulation are supported
     if ( grid%nnodeincell .eq. 3) then
        call graph%build_cellcell_graph(stderr, grid)
     else
        write(*,*) 'cell-cell connection not supported for this mesh' 
     end if
     
  end select

  !
  ! write to file
  !
  call file_graph%init(stderr, etb(path_graph),11,'out')
  call graph%write_mesh(stderr,file_graph)
  call file_graph%kill(stderr)

  !
  ! free memory
  !
  call grid%kill(stderr)
  call graph%kill(stderr)
     
  
end PROGRAM grid2graph

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc

  if (narg==1) then
     write(lun_err,*) ' Usage: '
  else
     write(lun_err,*) ' Error passing arguments, correct usage: '
  end if
  write(lun_err,*) '  ./grid2graph.out <grid>  <graph> <option> '
  write(lun_err,*) ' where :'
  write(lun_err,*) '  grid   (in ) : 2d triangulation in ascii of 2d grid'
  write(lun_err,*) '  graph  (out) : graph strusture '
  write(lun_err,*) '  option (in ) : "cell" or "node" '
  write(lun_err,*) '                  cell=> graph of cell-cell connection'
  write(lun_err,*) '                  node=> graph of node-node connection'
  if (narg==1) stop
  rc = IOerr(lun_err, err_inp, '', &
       ' Error reading argument number : ', narg)
end subroutine err_handle
