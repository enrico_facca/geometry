!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> Program to build uniformally refined grid from 2d, 3d and surface grid
!> It builds also a second file containing the relation betwewen the
!> indeces of the node and cell of the two triangualtion.
!> 
!> USAGE : see function err_handle
!>
!> Example:
!> file example_grid
!> 
!> 3 ! nnode
!> 1 ! ncell
!> 0.0 0.0 ! x y [z]
!> 1.0 0.0
!> 1.0 1.0 
!> 1 2 3 1 ! n1 n2 n3 material
!> 
!> nref=1
!>
!> file example_subgrid
!> 
!>           6  ! # of node 
!>           4           0  2d ! # of cells,  # of node in cell,  mesh type
!>   0.0        0.0        0.0     
!>   1.0        0.0        0.0     
!>   1.0        1.0        0.0     
!>  0.50        0.0        0.0     
!>   1.0       0.50        0.0     
!>  0.50       0.50        0.0     
!>           1           4           6           1
!>           2           4           5           1
!>           3           5           6           1
!>           4           5           6           1
!> 
!> example_parent  
!>           1             ! number of refiment
!>           3           6 ! nnode grid, nnode subgrid
!>           1           4 ! ncell grid, ncell_subgrid 
!>           1           1 ! n1   indeces of node generating node of sugrid (
!>           2           2 ! n2    if they coincides the node has the same coordinates,
!>           3           3 ! n3  otherwise the new node is the middle point of the
!>           1           2 ! n4 edges connecting the listed nodes
!>           2           3 ! n5
!>           3           1 ! n6
!>           1             ! c1 indeces of the cell that constians 
!>           1             ! c2 current subcell
!>           1             ! c3 
!>           1             ! c4
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!> 30 Dec 2020 - Changes due to work on build_edge_connection
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM uniform_refinement
  use Globals
  use AbstractGeometry
  use SparseMatrix

  implicit none
  
  type(abs_simplex_mesh), allocatable :: grids(:)
  type(file) :: fgrid
  type(file) :: fsubgrid
  type(file) :: fparent
  integer  :: nref
  logical  :: rc
  integer  :: res
  character(len=256) :: input,path_grid,path_subgrid,path_parent

  integer :: stderr,stdout,debug
  integer :: i, itria,err,narg,nargs
  integer :: ngrids,flag_reorder

  type(spmat) :: adj_matrix
  integer, allocatable :: perm(:),iperm(:)

  stderr=0
  stdout=6
  debug=6

  nargs = 0
  nargs = command_argument_count()
  if ( nargs < 4 )  call err_handle(stderr,narg)
  ! Geometry info
  ! Read original grid
  ! no renumbering
  ! no connection of second level
  narg=1
  call  get_command_argument(narg, path_grid,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
 
  narg=narg+1   
  call  get_command_argument(narg, input,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  read(input,*,iostat=res) nref
  if (res .ne. 0) call err_handle(stderr,narg)

  flag_reorder=0
  narg=narg+1   
  call  get_command_argument(narg, input,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  read(input,*,iostat=res) flag_reorder
  if (res .ne. 0) call err_handle(stderr,narg)
 
  narg=narg+1
  call  get_command_argument(narg, path_subgrid,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  if (nargs .eq. 5) then
     narg=narg+1
     call  get_command_argument(narg, path_parent,status=res)
     if (res .eq. 0) call fparent%init(stderr, etb(path_parent),30,'out')
  end if
  
  allocate(grids(0:nref),stat=res)
  if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'main', &
         ' subgrids',res)

  call fgrid%init(stderr, etb(path_grid),20,'in')
  call grids(0)%read_mesh(stderr,fgrid)
  write(stdout,'(a,I2,a,I8,a,I8)') ' Refinement # = ', 0,&
       ' Nnode= ', grids(0)%nnode,&
       ' Ncell= ', grids(0)%ncell
  !call grids(0)%info(stdout)
  call fgrid%kill(stderr)
  
  !
  ! refineing procedure
  !    
  do i=1,nref 
     call grids(i-1)%build_edge_connection(stderr)
     call grids(i)%refine(stderr,input_mesh=grids(i-1))
     write(stdout,'(a,I2,a,I8,a,I8)') ' Refinement # = ', i,&
          ' Nnode= ', grids(i)%nnode,&
          ' Ncell= ', grids(i)%ncell
     
     !
     ! write on the same file
     !
     if ( (i<nref) .and. ( fparent%exist ) ) call grids(i)%write_parent(stderr,fparent)
     !call grids(i)%info(stdout)
  end do

  select case (flag_reorder)
  case (1)
     !
     ! build adjency matrix
     !
     call grids(nref)%build_nodenode_matrix(stderr,.False.,adj_matrix)
     adj_matrix%coeff=one

     !
     ! build rcm permutation of graph node-node
     !
     allocate(perm(grids(nref)%nnode),iperm(grids(nref)%nnode))
     call adj_matrix%genrcm(6,perm,iperm)

     !
     ! reorder
     !
     call grids(nref)%renumber(stderr, grids(nref)%nnode,perm,iperm)    

     !
     ! free memory
     !
     call adj_matrix%kill(stderr)
     deallocate(perm,iperm,stat=res)

  case (2)
     if ( grids(nref)%nnodeincell .eq. 3 ) then
        call grids(nref)%build_cellcell_matrix(stderr,.False.,adj_matrix)
        adj_matrix%coeff=one

        !
        ! build rcm permutation of graph node-node
        !
        allocate(perm(grids(nref)%ncell),iperm(grids(nref)%ncell))
        call adj_matrix%genrcm(6,perm,iperm)

        !
        ! reorder
        !
        call grids(nref)%renumber(stderr, grids(nref)%ncell,perm,iperm)    

        !
        ! free memory
        !
        call adj_matrix%kill(stderr)
     else
        rc = IOerr(stderr, err_inp, 'main', &
             ' Cell Renumbering defined only for triangles =')
     end if
  case (3)
     !
     ! build adjency matrix
     !
     call grids(nref)%build_nodenode_matrix(stderr,.False.,adj_matrix)
     adj_matrix%coeff=one

     !
     ! build rcm permutation of graph node-node
     !
     allocate(perm(grids(nref)%nnode),iperm(grids(nref)%nnode))
     call adj_matrix%genrcm(6,perm,iperm)

     !
     ! reorder
     !
     call grids(nref)%renumber(stderr, grids(nref)%nnode,perm,iperm)    

     !
     ! free memory
     !
     call adj_matrix%kill(stderr)
     deallocate(perm,iperm,stat=res)

     !
     ! cell reorder
     !
     if ( grids(nref)%nnodeincell .eq. 3 ) then
        call grids(nref)%build_cellcell_matrix(stderr,.False.,adj_matrix)
        adj_matrix%coeff=one

        !
        ! build rcm permutation of graph node-node
        !
        allocate(perm(grids(nref)%ncell),iperm(grids(nref)%ncell))
        call adj_matrix%genrcm(6,perm,iperm)

        !
        ! reorder
        !
        call grids(nref)%renumber(stderr, grids(nref)%ncell,perm,iperm)    

        !
        ! free memory
        !
        call adj_matrix%kill(stderr)
     else
        rc = IOerr(stderr, err_inp, 'main', &
             ' Cell Renumbering defined only for triangles =')
     end if
  end select

  call fsubgrid%init(stderr, etb(path_subgrid),11,'out')
  call grids(nref)%write_mesh(stderr,fsubgrid)
  if( fparent%exist ) call grids(nref)%write_parent(stderr,fparent)
  call fsubgrid%kill(stderr)

  if( fparent%exist ) then
     call fparent%kill(stderr)
  end if
  
  !
  ! free memory
  !
  do i=1,nref 
     call grids(i)%kill(stderr)
  end do  
  


end PROGRAM uniform_refinement


subroutine err_handle(stderr, narg)
  use Globals
  implicit none

  integer, intent(in) :: stderr
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(stderr,*) ' Error passing arguments, correct usage:'
  write(stderr,*) ' ./refine_grid.out <grid> <nref> <subgrid> [<parent>]'
  write(stderr,*) ' where '
  write(stderr,*) ' grid    (in )          : triangulation in ascii'
  write(stderr,*) ' nref    (in )          : integer number of refinement  '
  write(stderr,*) ' reoder  (in )          : integer: reordering flag '
  write(stderr,*) '   0  do not reorder'
  write(stderr,*) '   1  reorder nodes '
  write(stderr,*) '   2  reorder cells '
  write(stderr,*) '   3  reorder nodes and cell '
  write(stderr,*) ' subgrid (out)          : refined triangulation in ascii'
  write(stderr,*) ' parent  (out,optional) : relation grid-subgrid in ascii'
  rc = IOerr(stderr, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
