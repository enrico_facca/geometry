!>---------------------------------------------------------------------
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> Program to renumber nodes or cells in 2d-3d-surface triangualtion
!> to reduces bandwitdth
!>
!> usage: 
!>       ./renumber.out <grid>  <what_reorder> <gridout> [<permutation>]
!> where :
!>  - grid          (in )      : triangulation in ascii
!>  - what_renumber (in )      : renumber 'cell' or 'node' 
!>  - gridout       (out)      : renumbered triangulation in ascii
!>  - permuation    (out,opt.) : relation between old and new numbering
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM renumber
  use Globals
  use SparseMatrix
  use AbstractGeometry
  implicit none
  
  type(file) :: fgrid
  type(file) :: fgridout
  type(file) :: fperm
  integer  :: nref
  logical  :: rc
  integer  :: res
  character(len=256) :: input,path_grid,path_gridout,path_permutation,command
  character(len=4) :: order_what

  integer :: stderr,stdout,debug,nargs
  integer :: i,j,err,narg,icell,iloc,lun_err,inode,iothernode,n1,n2
  integer :: ngrids,info
  integer :: nnz,m,nedge,nnode,ncell,nnodeincell
  type(spmat) :: adj_temp,adj_matrix,adj_perm,cell2cell_connection
  type(abs_simplex_mesh) :: grid
  integer, allocatable :: perm(:),iperm(:),topol_temp(:,:),nconn(:)
  integer, pointer :: neigh_cell(:,:),edges(:,:)

  stderr=0
  lun_err = 0

  ! Geometry info
  ! Read original grid
  ! no renumbering
  ! no connection of second level
  nargs=command_argument_count()
  if ( nargs < 3 ) call err_handle(stderr,1)
  

  err=0
  narg=1
  call  get_command_argument(narg, path_grid,status=res) 
  if (res .ne. 0) call err_handle(stderr,narg)

  narg=narg+1
  call  get_command_argument(narg, order_what,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
  if ( .not. ( ( etb(order_what) .eq. 'node') .or. &
       ( etb(order_what) .eq. 'cell' ) ) ) then
     call err_handle(stderr,narg)
  end if
   
  narg=narg+1   
  call  get_command_argument(narg, path_gridout,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
 
  if ( nargs .eq. 4 ) then
     narg=narg+1
     call  get_command_argument(narg, path_permutation,status=res)
     if (res .ne. 0) call err_handle(stderr,narg)
  end if

  call fgrid%init(stderr, etb(path_grid),20,'in')
  call grid%read_mesh(6,fgrid)
  call fgrid%kill(stderr)

  nnode       = grid%nnode
  ncell       = grid%ncell
  nnodeincell = grid%nnodeincell

  select case (etb(order_what))
  case ('node')
     !
     ! build adjency matrix
     !
     call grid%build_nodenode_matrix(stderr,.False.,adj_matrix)
     adj_matrix%coeff=one

     call adj_matrix%info(6)
     open(113,file='original.dat')
     call adj_matrix%write(113,'matlab')
     close(113)



     !
     ! build rcm permutation of graph node-node
     !
     allocate(perm(nnode),iperm(nnode))
     call adj_matrix%genrcm(6,perm,iperm)

     !
     ! reorder
     !
     call grid%renumber(stderr, nnode,perm,iperm)    

     !
     ! free memory
     !
     call adj_matrix%kill(stderr)
     deallocate(perm,iperm,stat=res)

  case ('cell')
     if ( nnodeincell .eq. 3 ) then
        call grid%build_cellcell_matrix(stderr,.False.,adj_matrix)
        adj_matrix%coeff=one

        !
        ! build rcm permutation of graph node-node
        !
        allocate(perm(ncell),iperm(ncell))
        call adj_matrix%genrcm(6,perm,iperm)

        !
        ! reorder
        !
        call grid%renumber(stderr, ncell,perm,iperm)    

        !
        ! free memory
        !
        call adj_matrix%kill(stderr)
     end if
  end select

  !
  ! write to file
  !
  call fgridout%init(stderr, etb(path_gridout),23,'out')
  call grid%write_mesh(stderr,fgridout)
  call fgridout%kill(stderr)

  !
  ! optionally write permutation
  !
  if (nargs .eq. 4) then
     call fperm%init(stderr, etb(path_permutation),24,'out')
     write(fperm%lun,*) size(perm),size(perm)
     do i=1,size(perm)
        write(fperm%lun,*) perm(i)
     end do
     call fperm%kill(stderr)
  end if


  !
  ! free memory
  !
  call grid%kill(6)
  
end PROGRAM renumber


subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc

  if (narg==1) then
     write(lun_err,*) ' Usage: '
  else
     write(lun_err,*) ' Error passing arguments, correct usage: '
  end if
  write(lun_err,*) '  ./renumber.out <grid>  <what_reorder> <gridout> [<permutation>]'
  write(lun_err,*) ' where '
  write(lun_err,*) '  grid          (in )      : triangulation in ascii'
  write(lun_err,*) '  renumber_what (in )      : renumber "cell" or "node" ' 
  write(lun_err,*) '  gridout       (out)      : renumbered triangulation in ascii '
  write(lun_err,*) '  permuation    (out,opt.) : relation between old and new numbering '
  if (narg==1) stop
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Error reading argument number : ', narg)
end subroutine err_handle

