!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> This program converts to vtk datas in timedata format 
!> (repository globals) associated to grids
!> 
!> Moduel Geometry2d is used for all tipo of Triangularization 
!> 2d, 3d and surface. 
!>
!> REVISION HISTORY:
!> dd mm  yyyy - 
!> 14 Gen 2019 - Correction to base program on repository structures 
!>
!> TODO_dd_mmm_yyyy - (TODO_name ) :TODO_describe_appropriate_changes 
!> 14 Gen 2019 - (Enrico Facca ) :  Program should use a general 
!>               module Geometry. It should be exteded or rewritten 
!>               to HDF5 file-format 
!<---------------------------------------------------------------------

PROGRAM streamline
  use Globals
  use AbstractGeometry  
  use TimeInputs
  use vtkloc

  implicit none
  type(abs_simplex_mesh)     :: grid
  type(file)     :: file_grid, file_data, file_vtk
  type(TimeData) :: data_input

  integer :: stderr,stdout,debug, flag_reading
  integer :: i,j,k, itria,itime,icell
  integer :: ndata,dim
  integer,allocatable :: data_dims(:)
  real(kind=double) :: time,dist
  real(kind=double) , allocatable :: utop(:),zeros(:)

  character(len=256) :: arg

  character(len=256) :: path_grid
  character(len=256) :: path_data
  character(len=256) :: data_type
  character(len=256) :: vtk_folder   
  character(len=256) :: fname
  character(len=256) :: name_data,vtk_name,var_name,scalar_vector=' '
  character(len=256), allocatable :: data_types(:), names_data(:)

  ! local vars
  integer :: u_number
  integer :: res,ncoord,nnodeincell,narg,res_read
  integer :: connection(5)
  logical :: rc
  character(len=256) :: rdwr,str,first_line,dst,base_name,dst_folder,dirname,basename,clean,msg

  logical endfile
  integer ppos,pbar,pdot,ndst,nstr,nargs
  integer :: vector_dimension,ndata_grid,ambient_dimension

  character(len=8) :: label

  real(kind=double) :: local_nodes_coordinate(3,4),nodes_coordinate(3,4)
  real(kind=double) :: point(3), local_point(3)
  real(kind=double) :: sum_length,sum_time
  real(kind=double) :: va(3),v1(3),v2(3)
  real(kind=double) :: field(3), local_field(3)
  real(kind=double) :: AC(3), AB(3), BC(3)
  real(kind=double) :: barycentric(4)
  real(kind=double) :: dnrm2,ddot
  
  stderr = 0

  nargs = command_argument_count() 
  narg = 0
  !if (nargs < 2) call err_handle(stderr,narg)
  
  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_grid
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  
  ! narg=narg+1
  ! call  get_command_argument(narg, arg,status=res)
  ! if (res .ne. 0) call err_handle(stderr,narg)
  ! read(arg,'(a256)',iostat=res_read) path_data
  ! if (res_read .ne. 0) call err_handle(stderr,narg)

 
  ! narg=narg+1
  ! call  get_command_argument(narg, arg,status=res)
  ! if (res .ne. 0) call err_handle(stderr,narg)
  ! read(arg,*,iostat=res_read) xpoint
  ! if (res_read .ne. 0) call err_handle(stderr,narg)
  
  ! narg=narg+1
  ! call  get_command_argument(4, arg,status=res)
  ! if (res .ne. 0) call err_handle(stderr,narg)
  ! read(arg,*,iostat=res_read) ypoint
  ! if (res_read .ne. 0) call err_handle(stderr,narg)
  
  ! narg=narg+1
  ! call  get_command_argument(narg, arg,status=res)
  ! if (res .ne. 0) call err_handle(stderr,narg)
  ! read(arg,*,iostat=res_read) zpoint
  ! if (res_read .ne. 0) call err_handle(stderr,narg)

  

  !
  ! open input files
  !
  call file_grid%init(stderr,etb(path_grid),10,'in')
  !call file_data%init(stderr, etb(path_data), 11,'in')

  
  !
  ! read grid
  !
  call grid%read_mesh(stderr,file_grid)
  ambient_dimension = grid%ambient_dimension
  call grid%build_bar_cell(stderr)
  call grid%build_size_cell(stderr)
  


  
  ! !
  ! ! get dimension data by the head
  ! !
  ! call data_input%init(stderr,file_data)
  ! dim=data_input%dimdata
  ! ndata=data_input%Ndata

  
  
  ! !
  ! ! check consistency
  ! !
  ! write(*,*) ndata,grid%nnode, grid%ncell,mod(ndata,grid%nnode),mod(ndata,grid%ncell)
  ! if (       ( mod(ndata,grid%nnode) .eq. 0 ) &
  !      .and. ( mod(ndata,grid%ncell) .eq. 0 ) ) then
  !    if (grid%ncell==grid%nnode) then
  !       rc = IOerr(stderr, err_inp , 'main', &
  !            'not able to tell if file '//etb(file_data%fn)//&
  !            'contians cell or node data for grid'//&
  !            etb(file_grid%fn))
  !    else
  !       if (ndata==grid%ncell) then
  !          vector_dimension=dim
  !          data_type='cell'
  !          ndata_grid=grid%ncell
  !       else
  !          vector_dimension=dim
  !          data_type='node'
  !          ndata_grid=grid%nnode
  !       end if
  !    end if
  ! else
  !    if ( mod(ndata,grid%nnode) .eq. 0 ) then
  !       vector_dimension=(ndata/grid%nnode)*dim
  !       data_type='node'
  !       ndata_grid=grid%nnode
  !    else
  !       if ( mod(ndata,grid%ncell) .eq. 0 ) then
  !          vector_dimension=(ndata/grid%ncell)*dim
  !          data_type='cell'
  !          ndata_grid=grid%ncell
  !       else
  !          rc = IOerr(stderr, err_inp , 'main', &
  !               'file '//etb(file_data%fn)//&
  !               ' has dimensions not consistent with grid '//&
  !               etb(file_grid%fn))
  !       end if
  !    end if
  ! end if
  
  write(*,*) vector_dimension,etb(data_type),ndata_grid,ndata


  sum_length = zero
  sum_time   =  zero
  point = (/-3,0,0/)
  ! find closest cell
  icell= 1
  dist=huge
  do i=1,grid%ncell
     if ( dist > dnrm2(3, grid%bar_cell(:,icell)-point,1)) then
        icell= i
        dist = dnrm2(3, grid%bar_cell(:,icell)-point,1)
     end if
  end do
  
  ! ! cycle until we are rather close to point p
  ! do while ( dnrm2(3, point-(/3,0,0/),1) > 0.1 ) 
  !    !
  !    call build_local_tangent_basis(grid,icell,v1,v2)

  !    ! copy local nodes coordinate
  !    do i=1, grid%nnodeincell
  !       nodes_coordinate(:,i)=grid%coord(:,grid%topol(i,icell))
  !    end do

     
  !    ! set A,B, C in local coordinate
  !    ! A=(0,0,0)
  !    local_nodes_coordinate(:,1)=zero
  !    ! B=(|AB|,0,0)
  !    local_nodes_coordinate(2,2)=dnrm2(3,nodes_coordinate(:,1)-nodes_coordinate(:,2),1)
  !    ! C=(Cx,Cy,0)
  !    AC=nodes_coordinate(:,1)-nodes_coordinate(:,3)
  !    local_nodes_coordinate(1,3)=ddot(3,v1,1,AC,1)
  !    local_nodes_coordinate(2,3)=ddot(3,v2,1,AC,1)
  !    local_nodes_coordinate(3,3)=zero
     
  !    ! convert vector field in local tangent coordinate
  !    local_field(1)=ddot(3,v1,1,field,1)
  !    local_field(2)=ddot(3,v2,1,field,1)

  !    ! compute point in barycentric coordinates
  !    call pure_cartesian2barycentric(&
  !      3,&
  !      nodes_coordinate,&
  !      point,&
  !      barycentric)

  !    ! compute local point
  !    local_point=&
  !         local_nodes_coordinates(:,1)*barycentric(1) + &
  !         local_nodes_coordinates(:,2)*barycentric(2) + &
  !         local_nodes_coordinates(:,3)*barycentric(3)

  !    !call  integrate_flux(local_nodes_coordinate(1:2,3), &
  !    ! local_point(1:2), local_field, icell, iface, sum_lenght, sum_time)

  !    ! find index of new cell touch
  !    iedge_global=grid%side_cnc(iface_loc,icell)
  !    cells=grid%plist(:,iedge_globals)
  !    new_cell=cells(1)
  !    if ( icell=cells(1)) new_cell=cells(2)
     
  !    ! checks
  !    ! check if direction of next flux

  !    ! get outward normal projected to new cell
  !    ! normal_cell=grid%normal_edge(iedge_global)
     

     
  !    ! compute new point in barycentric coordinates
  !    call pure_cartesian2barycentric(&
  !      3,&
  !      local_nodes_coordinate,&
  !      local_point,&
  !      barycentric)

  !    ! convert in global coordinate
  !    point=&
  !         nodes_coordinates(:,1)*barycentric(1) + &
  !         nodes_coordinates(:,2)*barycentric(2) + &
  !         nodes_coordinates(:,3)*barycentric(3)

  !    write(*,*) 'Icell', icell, 'bar', barycentric(1:3)

    
  ! end do
     
     
     
  
 end PROGRAM streamline

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./timedata2vtk_grid.out <grid> <data> [<vtk_dest> <var_name> <scalar_vector>]'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid     (in )      : triangulation in ascii'
  write(lun_err,*) ' data     (in )      : timedata to be converted defined on cell or nodes'
  write(lun_err,*) ' vtk_dest (in )      : 1 - if not passed vtk are created locally'
  write(lun_err,*) '          (optional) :     with same name of the data'
  write(lun_err,*) '                     : 2 - if a directory (end in "/") is passed '
  write(lun_err,*) '                     :     vtk files are created in "path"'
  write(lun_err,*) '                     :     with the same name of data'
  write(lun_err,*) '                     : 3 - if a path in form path/name.vtk is passed, '
  write(lun_err,*) '                     :     vtk files are in "path" with name "name"'
  write(lun_err,*) ' var_name (in )      : Variable name in vtk file'
  write(lun_err,*) '          (optional)'
  write(lun_err,*) ' -s or -v (in )      : how to handle vectorial data'
  write(lun_err,*) '          (optional)'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle

! subroutine integrate_flux(grid, point, field, icell, iface_local, lenght)
!   implicit none
!   type(abs_simplex_mesh) :: grid
!   real(kind=double) :: point(2)
!   real(kind=double) :: field(2)
!   integer :: icell, iface
  
 
!   ! field contians the local vector field
  
!   ! we return :
!   ! current point coordinate
!   ! current time=curve length/speed
!   ! current cell index
!   ! current face index  
! end subroutine integrate_flux

subroutine build_local_tangent_basis(grid,icell,v1,v2)
  use Globals
  use AbstractGeometry  

  implicit none
  type(abs_simplex_mesh) :: grid
  integer :: icell
  real(kind=double) :: v1(3)
  real(kind=double) :: v2(3)

  real(kind=double) :: ddot,dnrm2
  real(kind=double) :: va(3)
  integer :: i,j

  ! compute vector ortogonal to triangle
  v1=grid%coord(:,grid%topol(1,icell))-&
         grid%coord(:,grid%topol(2,icell))
  v2=grid%coord(:,grid%topol(1,icell))-&
       grid%coord(:,grid%topol(3,icell))
  va = cross(v1,v2)

  ! compute vector ortogonal to v1 in plane containg the triangle
  ! using the cross product between v1 and va 
  v2= cross(v1,va)

  ! nomralize
  v1=v1/dnrm2(3,v1,1)
  v2=v2/dnrm2(3,v2,1)

  
end subroutine build_local_tangent_basis
